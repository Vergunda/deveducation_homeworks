describe('JSON vizualize test', () => {

	describe('function visualizeJson() test 1 degree of nesting', () => {

			it(`should create elements and put them in DOM-tree`, () => {
				const jsonObject = {
					firstName: 'Dmytro',
					secondName: 'Verhun',
					status: 'student',
				};
				
				const root = document.createElement('div')
				const keySpan = document.createElement('span');
				const subDiv = document.createElement('div');
				keySpan.textContent = 'firstName' + ': ';
				subDiv.appendChild(keySpan);
				const valueSpan = document.createElement('span');
				valueSpan.textContent = 'Dmytro';
				subDiv.appendChild(valueSpan);
				root.appendChild(subDiv);
				
				const keySpan1 = document.createElement('span');
				const subDiv1 = document.createElement('div');
				keySpan1.textContent = 'secondName' + ': ';
				subDiv1.appendChild(keySpan1);
				const valueSpan1 = document.createElement('span');
				valueSpan1.textContent = 'Verhun';
				subDiv1.appendChild(valueSpan1);
				root.appendChild(subDiv1);
				
				const keySpan2 = document.createElement('span');
				const subDiv2 = document.createElement('div');
				keySpan2.textContent = 'status' + ': ';
				subDiv2.appendChild(keySpan2);
				const valueSpan2 = document.createElement('span');
				valueSpan2.textContent = 'student';
				subDiv2.appendChild(valueSpan2);
				root.appendChild(subDiv2);
				
				
				const expected = root;
				
				
				const actual = visualizeJson(jsonObject);
				assert.deepEqual(expected, actual);
			});
		});
	
	describe('function visualizeJson() test two degree of nesting', () => {
		
		it(`should create elements and put them in DOM-tree`, () => {
			const jsonObject = {
				firstName: 'Dmytro',
				secondName: 'Verhun',
				birthday: {
					year: 1986,
					month: 04,
					date: 24,
				}
				
			};
			
			const root = document.createElement('div')
			const keySpan = document.createElement('span');
			const subDiv = document.createElement('div');
			keySpan.textContent = 'firstName' + ': ';
			subDiv.appendChild(keySpan);
			const valueSpan = document.createElement('span');
			valueSpan.textContent = 'Dmytro';
			subDiv.appendChild(valueSpan);
			root.appendChild(subDiv);
			
			const keySpan1 = document.createElement('span');
			const subDiv1 = document.createElement('div');
			keySpan1.textContent = 'secondName' + ': ';
			subDiv1.appendChild(keySpan1);
			const valueSpan1 = document.createElement('span');
			valueSpan1.textContent = 'Verhun';
			subDiv1.appendChild(valueSpan1);
			root.appendChild(subDiv1);
	
			const keySpan2 = document.createElement('span');
			const subDiv2 = document.createElement('div');
			keySpan2.textContent = 'birthday' + ': ';
			subDiv2.appendChild(keySpan2);
			const subRoot2 = document.createElement('div');
			subDiv2.appendChild(subRoot2);
			createDiv({
				year: 1986,
				month: 04,
				date: 24,
			}, subRoot2);
			root.appendChild(subDiv2);
			
			
			const expected = root ;
			
			
			const actual = visualizeJson(jsonObject);
			assert.deepEqual(expected, actual);
		});
	});
	
	describe('function visualizeJson() test two degree of nesting with Array', () => {
		
		it(`should create elements and put them in DOM-tree`, () => {
			const jsonObject = {
				firstName: 'Dmytro',
				secondName: 'Verhun',
				birthday: {
					year: 1986,
					month: 04,
					date: 24,
				},
				hobbies: [
					'music',
					'football',
				],
			};
			
			const expected =
				'<div>' +
					'<div>' +
						'<span>firstName: </span>' +
						'<span>Dmytro</span>' +
					'</div>' +
					'<div>' +
						'<span>secondName: </span>' +
						'<span>Verhun</span>' +
					'</div>' +
					'<div>' +
						'<span>birthday: </span>' +
							'<div>' +
								'<div>' +
									'<span>year: </span>' +
									'<span>1986</span>' +
								'</div>' +
								'<div>' +
									'<span>month: </span>' +
									'<span>4</span>' +
								'</div>' +
								'<div>' +
									'<span>date: </span>' +
									'<span>24</span>' +
								'</div>'+
							'</div>' +
						'<span>hobbies: </span>' +
							'<div>' +
								'<div>' +
									'<span>0: </span>' +
									'<span>music</span>' +
								'</div>' +
								'<div>' +
									'<span>1: </span>' +
									'<span>football</span>' +
								'</div>' +
							'</div>' +
					'</div>' +
				'</div>';

			// const root = document.createElement('div');
			//
			// const keySpan = document.createElement('span');
			// const subDiv = document.createElement('div');
			// keySpan.textContent = 'firstName' + ': ';
			// subDiv.appendChild(keySpan);
			// const valueSpan = document.createElement('span');
			// valueSpan.textContent = 'Dmytro';
			// subDiv.appendChild(valueSpan);
			// root.appendChild(subDiv);
			//
			// const keySpan1 = document.createElement('span');
			// const subDiv1 = document.createElement('div');
			// keySpan1.textContent = 'secondName' + ': ';
			// subDiv1.appendChild(keySpan1);
			// const valueSpan1 = document.createElement('span');
			// valueSpan1.textContent = 'Verhun';
			// subDiv1.appendChild(valueSpan1);
			// root.appendChild(subDiv1);
			//
			// const keySpan2 = document.createElement('span');
			// const subDiv2 = document.createElement('div');
			// keySpan2.textContent = 'birthday' + ': ';
			// subDiv2.appendChild(keySpan2);
			// const subRoot2 = document.createElement('div');
			// subDiv2.appendChild(subRoot2);
			// createDiv({
			// 	year: 1986,
			// 	month: 04,
			// 	date: 24,
			// }, subRoot2);
			// root.appendChild(subDiv2);
			//
			// const keySpan3 = document.createElement('span');
			// const subDiv3 = document.createElement('div');
			// keySpan3.textContent = 'hobbies' + ': ';
			// subDiv3.appendChild(keySpan3);
			// const subRoot3 = document.createElement('div');
			// subDiv3.appendChild(subRoot3);
			// createDiv([
			// 	'music',
			// 	'football',
			// ], subRoot3);
			// root.appendChild(subDiv3);

			const expected = root;

			const actual = visualizeJson(jsonObject).toString();
			assert.equal(expected, actual);
		});
	});
});

// <div><div><span>firstName: </span><span>Dmytro</span></div>
// <div><span>secondName: </span><span>Verhun</span></div>
// <div><span>birthday: </span><div><div><span>year: </span>
// <span>1986</span></div>
// <div><span>month: </span><span>4</span></div><div>
// <span>date: </span><span>24</span></div>
// </div><span>student</span></div></div>
// <div><div><span>firstName: </span><span>Dmytro</span></div>
// <div><span>secondName: </span><span>Verhun</span></div>
// <div><span>birthday: </span><div><div>
// <span>year: </span>
// <span>1986</span></div>
// <div><span>month: </span><span>4</span></div><div>
// <span>date: </span><span>24</span></div></div></div></div>
