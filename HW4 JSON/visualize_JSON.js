testObject = {
    "status": "busy",
    "version": {
        "last": "3.1",
        "test": "3.2",
    },
    "message": "you have no messages",
    "person": {
        "firstName": "Dima",
        "secondName": "Verhun",
        "birthday": {
            "year": "1986",
            "date": "04/11",
        }
    },
    "array": [
        "test-1",
        "test-2",
    ],
};

function clear() {
    if (document.getElementById("myDiv").querySelector(".firstBlock") != null) {
        document.getElementById("myDiv").querySelector(".firstBlock").remove();
    }
}

function hideInnerBlock(event) {

    const target = event.target;

    if (!target.hasAttribute("triangle" )) {
        return
    };

    if (target.hasAttribute("triangle" )) {

    }
    // buffer = document.getElementById("myDiv").querySelector(".firstBlock").querySelector(".secondBlock");
    // if (document.getElementById("myDiv").querySelector(".firstBlock").querySelector(".secondBlock") != null) {
    //     document.getElementById("myDiv").querySelector(".firstBlock").querySelector(".secondBlock").remove();
    // } else {
    //     document.getElementById("myDiv").querySelector(".firstBlock").append(buffer);
    // }
}

function hideJSONView() {
    if (document.getElementById("myDiv").querySelector(".JSON--firstBlock") != null) {
        document.getElementById("myDiv").querySelector(".JSON--firstBlock").remove();
    } else {
       run();
    }
}


//run function:
function run() {
    // const object = JSON.parse(document.getElementById('JSONInput').value);
    clear();
    const object = testObject;
    visualizeJson(object);
    append(visualizeJson(object));
    
}

//append our root to last element of body
function append(root) {
    document.getElementById("myDiv").append(root);
}

//creating new div
function createDiv(jsonObject, root, classOfBlock) {
    
    for (let key in jsonObject) {
        const keySpan = document.createElement('span');
        keySpan.classList.add(classOfBlock);
        const subDiv = document.createElement('div');
        subDiv.classList.add(classOfBlock);
        if (typeof  jsonObject[key] === 'object') {
            const triangle = document.createElement('span');
            triangle.innerHTML = "&#9660;";
            triangle.setAttribute("onclick", "hideInnerBlock()");
            triangle.setAttribute("data", "triangle");
            subDiv.appendChild(triangle);
        }
        keySpan.textContent = key + ': ';
        subDiv.appendChild(keySpan);
        
        if (typeof jsonObject[key] === 'object') {
            const subRoot = document.createElement('div');
            subDiv.appendChild(subRoot);
            const classOfInnerBlock = defineClass(classOfBlock);
            
            createDiv(jsonObject[key], subRoot, classOfInnerBlock);
        } else {
            const valueSpan = document.createElement('span');
            valueSpan.classList.add(classOfBlock);
            valueSpan.textContent = jsonObject[key];
            subDiv.appendChild(valueSpan);
        }
  
        root.classList.add(classOfBlock);
        root.appendChild(subDiv);
    }
}

// Define SCC class of blocks
defineClass = (classOfBlock) => classOfBlock === 'JSON--firstBlock' ? 'JSON--secondBlock' : 'JSON--firstBlock';

//creating root - div with inner JSON object
function visualizeJson(jsonObject) {
    const root = document.createElement('div');
    const classOfBlock = defineClass(document.getElementById("myDiv").className);
    createDiv(jsonObject, root, classOfBlock);
    return root;
}