class ArrayList {
  constructor() {
    this.array = [];
  }
  
  clear() {
    this.array = [];
  }
  
  init(array) {
    for (let i = 0; typeof (array[i]) === "number"; i++) {
      this.array[i] = array[i];
    }
  }

  toString() {

    for(cin)
    return '{' + this.array + '}';
  }
  
  getSize() {
    let size = 0;
    for (let i = 0; typeof (this.array[i]) === 'number'; i++) {
      size++;
    }
    return size;
  }
  
  
  push(value) {
    this.array[this.getSize()] = value;
  }

  pop() {
    const value = this.array[this.getSize() - 1];
    this.array[this.getSize() - 1] = undefined;
    return value;
  }
  
  shift() {
    const value = this.array[0];
    for (let i = 1; i < this.getSize(); i++) {
      this.array[i - 1] = this.array[i];
    }
    this.array[this.getSize() - 1] = undefined;
    return value;
  }
  
  unshift(value) {
    for (let i = this.getSize(); i > 0; i--) {
      this.array[i] = this.array[i - 1];
    }
    this.array[0] = value;
  }
  
  set(index, value) {
    for (let i = this.getSize(); i >= index; i--) {
      this.array[i] = this.array[i - 1];
    }
    this.array[index] = value;
  }
  
  get(index) {
    return this.array[index];
  }
  
  sort(a, b) {
  
    let temp;
    const length = this.getSize();
    
    if (a > b) {
      for (let i = 0; i < length; i++) {
        for (let j = 0; j < length - i; j++) {
          if (this.array[j] < this.array[j + 1]) {
            temp = this.array[j + 1];
            this.array[j + 1] = this.array[j];
            this.array[j] = temp;
          }
        }
      }
    } else {
            for (let i = 0; i < length; i++) {
              for (let j = 0; j < length - i; j++) {
                if (this.array[j] > this.array[j + 1]) {
                  temp = this.array[j + 1];
                  this.array[j + 1] = this.array[j];
                  this.array[j] = temp;
                }
              }
            }
          }
    return this.array;
  }
}
      
let arrayList = new ArrayList();