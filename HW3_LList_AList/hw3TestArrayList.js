describe('ArrayList Tests', () => {
    
    describe('getSize() - returns current size of list', () => {
        const testData = [
            {
                array: [],
                expected: 0,
            },
            {
                array: [1],
                expected: 1,
            },
            {
                array: [-5, 0],
                expected: 2,
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: 5
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: 6
            },
        ];
        it(`should return 0 when array []`, () => {
            const {array, expected} = testData[0];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.getSize();
            
            assert.strictEqual(actual, expected);
        });
        it(`should return 1 when array [1]`, () => {
            const {array, expected} = testData[1];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.getSize();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return 2 when array [-5,0]`, () => {
            const {array, expected} = testData[2];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.getSize();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return 5 when array [-5, 20, 59, 0, 13]`, () => {
            const {array, expected} = testData[3];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.getSize();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return 6 when array [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected} = testData[4];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array)
            const actual = arrayList.getSize();
    
            assert.strictEqual(actual, expected);
        });
        
    });
    
    describe('toString() - returns string in format \'{ 1, 2, 3, 4 }', () => {
        const testData = [
            {
                array: [],
                expected: '{}',
            },
            {
                array: [1],
                expected: '{1}',
            },
            {
                array: [-5, 0],
                expected: '{-5,0}',
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: '{-5,20,59,0,13}'
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: '{-5,20,59,0,13,3}'
            },
        ];
        it(`should return [] when array = '{}'`, () => {
            const {array, expected} = testData[0];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.toString();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return [1] when array = '{1}'`, () => {
            const {array, expected} = testData[1];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.toString();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 0] when array = '{-5, 0}'`, () => {
            const {array, expected} = testData[2];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.toString();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13] when array = '{-5,20,59,0,13}'`, () => {
            const {array, expected} = testData[3];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.toString();
    
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13, 3] when array = '{-5,20,59,0,13,3}'`, () => {
            const {array, expected} = testData[4];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.toString();
    
            assert.strictEqual(actual, expected);
        });
    });
    
    describe('push(value) - add element to the end of list', () => {
        const testData = [
            {
                array: [],
                value: 1,
                expected: 1,
                size: 1,
            },
            {
                array: [1],
                value: 2,
                expected: 2,
                size: 2,
            },
            {
                array: [-5, 0],
                value: 3,
                expected: 3,
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                expected: 4,
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                expected: 5,
                size: 7,
            },
        ];
        
        it(`should return value: 1 and size 1, when array = [] and value = 1`, () => {
            const {array, value, expected, size} = testData[0];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.push(value);
            const actual = arrayList.array[arrayList.getSize() - 1];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 2 and size 2, when array = [1] and value = 2`, () => {
            const {array, value, expected, size} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.push(value);
            const actual = arrayList.array[arrayList.getSize() - 1];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 3 and size 3, when array = [-5, 0] and value = 3`, () => {
            const {array, value, expected, size} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.push(value);
            const actual = arrayList.array[arrayList.getSize() - 1];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 4, , when array = [-5, 20, 59, 0, 13] and value = 4`, () => {
            const {array, value, expected, size} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.push(value);
            const actual = arrayList.array[arrayList.getSize() - 1];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 5, when array = [-5, 20, 59, 0, 13, 3] and value = 5`, () => {
            const {array, value, expected, size} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.push(value);
            const actual = arrayList.array[arrayList.getSize() - 1];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
    });
    
    describe('pop() - delete element from end and returns it value', () => {
        const testData = [
            {
                array: [],
                expected: undefined,
                lastItem: undefined,
                size: 0,
            },
            {
                array: [1],
                expected: 1,
                lastItem: undefined,
                size: 0,
            },
            {
                array: [-5, 0],
                expected: 0,
                lastItem: -5,
                size: 1,
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: 13,
                lastItem: 0,
                size: 4,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: 3,
                lastItem: 13,
                size: 5,
            },
        ];
        
        it(`should return value = null, lastItem = null and size 0, when array = []`, () => {
            const {array, expected, lastItem, size} = testData[0];
     
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.pop();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(lastItem, arrayList.array[arrayList.getSize() - 1]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 1, lastItem = null and size 0, when array = [1]`, () => {
            const {array, expected, lastItem, size} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.pop();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(lastItem, arrayList.array[arrayList.getSize() - 1]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 0, lastItem = -5 and size 1, when array = [-5, 0]`, () => {
            const {array, expected, lastItem, size} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.pop();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(lastItem, arrayList.array[arrayList.getSize() - 1]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 13, lastItem = 0 and size 4 when array = [-5, 20, 59, 0, 13]`, () => {
            const {array, expected, lastItem, size} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.pop();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(lastItem, arrayList.array[arrayList.getSize() - 1]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 3, lastItem = 13 and size 5, when array = [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected, lastItem, size} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.pop();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(lastItem, arrayList.array[arrayList.getSize() - 1]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
    });
    
    describe('shift() - delete element from start and returns it value', () => {
        const testData = [
            {
                array: [],
                expected: undefined,
                head: undefined,
                size: 0,
            },
            {
                array: [1],
                expected: 1,
                head: undefined,
                size: 0,
            },
            {
                array: [-5, 0],
                expected: -5,
                head: 0,
                size: 1,
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: -5,
                head: 20,
                size: 4,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: -5,
                head: 20,
                size: 5,
            },
        ];
        
        it(`should return value = null, head = null and size 0, when array = []`, () => {
            const {array, expected, head, size} = testData[0];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.shift();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(head, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: 1, head = null and size 0, when array = [1]`, () => {
            const {array, expected, head, size} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.shift();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(head, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: -5, head = 0 and size 1, when array = [-5, 0]`, () => {
            const {array, expected, head, size} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.shift();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(head, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: -5, head = 20 and size 4 when array = [-5, 20, 59, 0, 13]`, () => {
            const {array, expected, head, size} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.shift();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(head, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return value: -5 head = 20 and size 5, when array = [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected, head, size} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.shift();
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(head, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
    });
    
    describe('unshift(value) - add element to the start of list', () => {
        const testData = [
            {
                array: [],
                value: 1,
                expected: 1,
                size: 1,
            },
            {
                array: [1],
                value: 2,
                expected: 2,
                size: 2,
            },
            {
                array: [-5, 0],
                value: 3,
                expected: 3,
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                expected: 4,
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                expected: 5,
                size: 7,
            },
        ];
        
        it(`should return head.value = 1 and size 1, when array = [] and value 1`, () => {
            const {array, value, expected, size} = testData[0];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.unshift(value);
    
            assert.strictEqual(expected, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return head.value: 2 and size 2, when array = [1] and value 2`, () => {
            const {array, value, expected, size} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.unshift(value);
    
            assert.strictEqual(expected, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return head.value: 3 and size 3, when array = [-5, 0] and value = 3`, () => {
            const {array, value, expected, size} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.unshift(value);
    
            assert.strictEqual(expected, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return head.value: 4 and size 6 when array = [-5, 20, 59, 0, 13] and value = 4`, () => {
            const {array, value, expected, size} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.unshift(value);
    
            assert.strictEqual(expected, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return head.value: 5 and size 7, when array = [-5, 20, 59, 0, 13, 3] and value = 5`, () => {
            const {array, value, expected, size} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.unshift(value);
    
            assert.strictEqual(expected, arrayList.array[0]);
            assert.strictEqual(size, arrayList.getSize());
        });
    });
    
    describe('get(index) - set value by index', () => {
        const testData = [
            {
                array: [],
                index: 0,
                expected: undefined,
            },
            {
                array: [1],
                index: 1,
                expected: undefined,
            },
            {
                array: [-5, 0],
                index: 1,
                expected: 0,
            },
            {
                array: [-5, 20, 59, 0, 13],
                index: 2,
                expected: 59,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                index: 4,
                expected: 13,
            },
        ];
        
        it(`should return undefined, when array = [] and index = 1`, () => {
            const {array, index, expected} = testData[0];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.get(index);
    
            assert.strictEqual(actual, expected);
        });
        
        it(`should return undefined, when array = [1] and index = 1`, () => {
            const {array, index, expected} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.get(index);
    
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 0, when array = [-5, 0] and index = 1`, () => {
            const {array, index, expected} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.get(index);
    
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 59, when array = [-5, 20, 59, 0, 13],and index = 2`, () => {
            const {array, index, expected} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.get(index);
    
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 13, when array = [-5, 20, 59, 0, 13, 3],and index = 4`, () => {
            const {array, index, expected} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.get(index);
    
            assert.strictEqual(actual, expected);
        });
        
    });
    
    describe('set(index, value) - set value by index', () => {
        const testData = [
            {
                array: [],
                value: 1,
                index: 0,
                expected: 1,
                size: 1,
            },
            {
                array: [1],
                value: 2,
                index: 2,
                expected: 2,
                size: 1,
            },
            {
                array: [-5, 0],
                value: 3,
                index: 1,
                expected: 3,
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                index: 3,
                expected: 4,
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                index: 4,
                expected: 5,
                size: 7,
            },
        ];
        
        it(`should return 1 and size 1, when array = [] and value = 1, index = 0`, () => {
            const {array, value, index, expected, size} = testData[0];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.set(index, value);
            const actual = arrayList.array[index];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return undefined and size 2, when array = [1] and value = 2, index = 2`, () => {
            const {array, value, index, expected, size} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.set(index, value);
            const actual = arrayList.array[index];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return 3 and size 3, when array = [-5, 0] and value = 3, index = 1`, () => {
            const {array, value, index, expected, size} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.set(index, value);
            const actual = arrayList.array[index];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return 4 and size 6, when array = [-5, 20, 59, 0, 13] and value = 4, index = 3`, () => {
            const {array, value, index, expected, size} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.set(index, value);
            const actual = arrayList.array[index];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
        it(`should return 5 and size 7, when array = [-5, 20, 59, 0, 13, 3] and value = 5, index = 4`, () => {
            const {array, value, index, expected, size} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            arrayList.set(index, value);
            const actual = arrayList.array[index];
    
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, arrayList.getSize());
        });
        
    });
    
    describe('sort(a, b) - returns string in format \'{ 1, 2, 3, 4 }', () => {
        const testData = [
            {
                array: [],
                a: 1,
                b: 2,
                expected: [],
            },
            {
                array: [1],
                a: 1,
                b: 2,
                expected: [1],
            },
            {
                array: [-5, 0],
                a: 3,
                b: 2,
                expected: [0, -5],
            },
            {
                array: [-5, 20, 59, 0, 13],
                a: 1,
                b: 2,
                expected: [-5, 0, 13, 20, 59],
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                a: 3,
                b: 2,
                expected: [59, 20, 13, 3, 0, -5],
            },
        ];
        it(`should return [] when array = [], a = 1, b = 2`, () => {
            const {array, a, b, expected} = testData[0];
            
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.sort(a, b);
            
            assert.deepEqual(actual, expected);
        });
        it(`should return [1] when array = [1], a = 1, b = 2`, () => {
            const {array, a, b, expected} = testData[1];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.sort(a, b);
    
            assert.deepEqual(actual, expected);
        });
        it(`should return [-5, 0] when array = [0, -5], a = 3, b = 2`, () => {
            const {array, a, b, expected} = testData[2];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.sort(a, b);
    
            assert.deepEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13] when array = [-5, 0, 13, 20, 59], a = 1, b = 2`, () => {
            const {array, a, b, expected} = testData[3];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.sort(a, b);
    
            assert.deepEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13, 3] when array = [59, 20, 13, 3, 0, -5], a = 3, b = 2`, () => {
            const {array, a, b, expected} = testData[4];
    
            arrayList.clear();
            arrayList = new ArrayList();
            arrayList.init(array);
            const actual = arrayList.sort(a, b);
    
            assert.deepEqual(actual, expected);
        });
    });
});
