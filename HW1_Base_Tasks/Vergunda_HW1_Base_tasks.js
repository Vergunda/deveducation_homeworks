const sumOrMultiply = (a, b) => a % 2 === 0 ? a * b : a + b;

function quaterOfCoordinates(x, y)	{
	if (x > 0 && y > 0) {
	return 1;
		} else if (x < 0 && y > 0 ){
	return 2;
		} else if (x < 0 && y < 0 ){
	return 3;
		} else if (x > 0 && y < 0 ){
	return 4;
		} if (x === 0 || y === 0){
	return "You are in coordinate axis";
	}
};

function positiveNumberSum(a,b,c) {
  let sum = 0;
  if (a > 0) {sum = sum + a};
  if (b > 0) {sum = sum + b};
  if (c > 0) {sum = sum + c};
  return sum;
}

const maxSumOrMultiply = (a,b,c) => (a + b + c) >= (a * b * c) ? (a + b + c + 3): (a * b * c + 3);

function rating(score) {
    if ( score >= 0 && score < 20) {return "F"
    } else if ( score >= 20 && score < 40 ) { return "E"
    } else if ( score >= 40 && score < 60 ) { return "D"
    } else if ( score >= 60 && score < 75 ) { return "C"
    } else if ( score >= 75 && score < 90 ) { return "B"
    } else if ( score >= 90 && score <= 100 ) { return "A"
		} else {throw "You input wrong number"}
}

function paritySumAndAmount()	{
	let sumParity = 0;
	let amountParity = 0;
	for (i = 1; i < 100; i++) 	{
			if (i % 2 == 0)	{
				sumParity = sumParity + i;
				amountParity = amountParity++;
			}
		}
	return [sumParity, amountParity];
}

function isSimpleNumber(number) {
		let isSimple;
		if (number = 2) {
			isSimple = true;
		} else {
		for (i = 2; i <= number; i++) {
			if (number % i === 0) {
				 isSimple = false;
				 break;
			 } else {
				 isSimple = true;
			 }
	   }
		}
	 return isSimple;
	}

function integerSquare(number) {
	let i = 0;
		while (i*i < number) { i++;}
		if (Math.pow((i*i - (i - 1)*(i - 1))/2,2) > number) {
			square = i - 1;
		} else { square = i; }
	return square;
}

const countFactorial = (n) => n <= 0 ? 1 : n*countFactorial(n - 1);

function factorial(n) {
	let factorial = 1;
	for (i = 1; i <= n; i++) {
		factorial = factorial * i;
	}
	return factorial;
}

function sumOfDigits(number) {
	 let sum = 0;
	 let mod = 0;
			while (number > 0) {
  			mod = number % 10;
  			sum = sum + mod;
  			number = (number - +mod) / 10;
			}
		return sum;
	}

function mirrorDigits(number) {
	let inverse = 0;
	let digit = 1;
	for (i = number.toString().length; i > 0; i--) {
					for (j = i - 1; j > 0; j--) {
						digit = digit * 10;
					}
			inverse = inverse + (number % 10) * digit;
			number = (number - (number % 10)) / 10;
			digit = 1;
	}
  return inverse;
}

function minArrayElement(arr) {
	let min = arr[0];
	for (i = 1; i < arr.length; i++) {
		if (arr[i] < min) {
			min = arr[i];
		}
	}
	return min;
}

function minArrayElement(arr) {
	let max = arr[0];
	for (i = 1; i < arr.length; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	return max;
}

function minIdArrayElement(arr) {
	let min = 0;
	for (i = 1; i < arr.length; i++) {
		if (arr[i] < arr[min]) {
			min = i;
		}
	}
	return min;
}

function maxIdArrayElement(arr) {
	let max = 0;
	for (i = 1; i < arr.length; i++) {
		if (arr[i] > arr[max]) {
			max = i;
			}
	  }
	return max;
	}

function sumNoParityId(arr) {
	let sum = 0;
	for (i = 0; i < arr.length; i++) {
		if (i % 2 != 0) {
			sum = sum + arr[i];
		}
	}
	return sum;
}

function reverseArray(arr) {
	let reverse = [];
	for (i = 0; i <= (arr.length - 1); i++) {
		reverse[i] = arr[(arr.length - 1)-i]
	}
	return reverse;
}

function numberOfNoParity(arr) {
  let amount= 0;
	for (i = 0; i < arr.length; i++) {
		if (arr[i] % 2 != 0) {
		 amount = amount + 1;
		}
	}
	return amount;
}

function changeArrayElements(arr) {
	for (i = 0; i < arr.length/2 ; i++) {
			arr.push(arr[0]);
			arr.shift()
		 }
	return arr;
}

function sortArrayBubble(arr) {
  let temp;
		for (j = arr.length; j > 0; j--) {
			for (i = 1; i < j; i++) {
				 if (arr[i - 1] > arr[i])	{
						temp = arr[i];
						arr[i] = arr[i - 1];
						arr[i - 1] = temp;
					}
			}
		}
		return arr;
}

function sortArrayInsert(arr) {
	let temp;
	for (j = 1; j < arr.length; j++) {
		temp = arr[j];
		i = j - 1;
		while (i >= 0 && arr[i] > temp)	{
        arr[i + 1] = arr[i];
				i--;
  		}
  		arr[i + 1] = temp;
  	}
  return arr;
}

function sortArraySelect(arr) {
	let temp;
	let j;
    for (i = 0; i < arr.length; i++) {
    		j = i;
    		for (k = i; k < arr.length; k++)
    			{if (arr[j] > arr[k]) {
						j = k;
					}
    		}
    		temp = arr[i];
    		arr[i] = arr[j];
    		arr[j] = temp;
    	}
		return arr;
	}

function sortArrayQuick(arr) {
    if (arr.length === 0) return [];
		let leftArray = [];
		let rightArray = [];
		let pivot = arr[0];
    for (i = 1; i < arr.length; i++) {
			 if (arr[i] < pivot) {
			 leftArray[leftArray.length] = arr[i];
		 } else {
			 rightArray[rightArray.length] = arr[i];
     }
	 return sortArrayQuick(leftArray).concat(pivot,sortArrayQuick(rightArray));
	}
}

/*function sortArrayMerge(arr) {
	if (arr.length == 1) {
		return arr;
  } else {
		let leftArray = [];
		let rightArray = [];
		for (i = 0; i < arr.length; i++) {
			if (i < arr.length/2) {
				leftArray[i] = arr[i];
			} else {
				rightArray[i] = arr[i];
			}
		}
	leftArray = sortArrayMerge(leftArray);
	rightArray = sortArrayMerge(rightArray);
  return merge(leftArray,rightArray);
	}
}*/
/*
function merge(leftArray,rightArray) {
	let sortedArray = [];
	while (leftArray.length >= 0|| rightArray.length >=0) {
		if (leftArray[0] > rightArray[0]) {
			sortedArray.push(rightArray[0]);
			rightArray.shift();
		} else {
			sortedArray.push(leftArray[0]);
			leftArray.shift();
		}
	}
	while (leftArray.length >= 0) {
		sortedArray.push(leftArray[0]);
		leftArray.shift();
	}
	while (rightArray.length >= 0) {
		sortedArray.push(rightArray[0]);
		rightArray.shift();
	}
	return sortedArray;
}

function HeapSort(A) {
    if (A.length == 0) return [];
    var n = A.length, i = Math.floor(n/2), j, k, t;
    while (true)
    { if (i > 0) t = A[--i];
      else { n--;
             if (n == 0) return A;
             t = A[n];  A[n] = A[0];
           }
      j = i;  k = j*2+1;
      while (k < n)
       { if (k+1 < n && A[k+1] > A[k]) k++;
         if (A[k] > t)
          { A[j] = A[k];  j = k;  k = j*2+1; }
         else break;
       }
      A[j] = t;
    }
}

	function Merge(a,low,mid,high)  {
	    var b = new Array(high+1-low), h, i, j = mid+1, k, h = low, i = 0;
	    while (h <= mid && j <= high )
	     { if (a[h] <= a[j]){ b[ i ]=a[h]; h++; }
	       else             { b[ i ]=a[j]; j++; }
	       i++;
	     }
	    if (h > mid){ for (k = j; k <= high; k++){ b[ i ]=a[k]; i++; } }
	    else        { for (k = h; k <= mid; k++){  b[ i ]=a[k]; i++; } }
	    for (k=0; k<=high-low; k++) a[k+low]=b[k];
	    return a;
	}

	function MergeSort(A) {
	    function merge_sort(a,low,high)
	     { if (low < high)
	        { var mid = Math.floor((low+high)/2);
	          merge_sort(a, low, mid);
	          merge_sort(a, mid+1, high);
	          Merge(a, low, mid, high);
	        }
	     }
	    var n = A.length;
	    merge_sort(A, 0, n-1);
	    return A;
	}
*/
function sortArrayShell(arr) {
    let pivotId = Math.floor(arr.length/2);
		let temp;
		 while (pivotId > 0) {
			for (j = 0; j < arr.length; j++) {
				let k = j;
				temp = arr[j];
          while (k >= pivotId && arr[k-i] > temp)
           { arr[k] = arr[k-i]; k -= i; }
          arr[k] = temp;
        }
      pivotId = (pivotId==2) ? 1 : Math.floor(pivotId*5/11);
     }
    return arr;
}

const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const dayOfWeek = (n) => ( (n ^ 0) === n && n > 0 && n < 8) ? week[n - 1] : console.log ("You input wrong number");

const lengthBetweenDots = (x1,y1,x2,y2) => Math.sqrt((x1 - x2)**2 + (y1 - y2)**2);



//Classwork

function getObject(arr) {
  const obj = {};
  for (i = 0; i < arr.length; i++) {
    obj[arr[i]] = 0;
  }
 return obj;
}

const getResult = () => Math.random() > 0.5 ? 'URA': 'NE URA';

const getType = (param) => typeof(param);

const getWholeString = (a,b,c) => a + b + c;
