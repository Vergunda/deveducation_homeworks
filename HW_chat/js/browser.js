let socket = new WebSocket('ws://localhost:3000');
let clientMessage = {
	typeOfData: null,
	client: null,
	message: null,
	chatId: null,
};
const messageContainers = document.getElementById('messageContainers');
const logInName = document.getElementById('logInName');
const clientLists = document.getElementById('clientLists');
const loginForm = document.getElementById("loginForm");
const form = document.getElementById("form");
const messageInput = document.getElementById('message');
const status = document.getElementById('status');
const chatLists = document.getElementById('chatLists');
const chooseChat = document.getElementsByName('chooseChat');
const createChatForm = document.getElementById("createChatForm");
let chatsBuffer;
document.getElementById('createChat');
let activeChatId = 0;

socket.onmessage = function (event) {
onMessage(event.data);
};

socket.onopen = function () {
	setStatus('online');
};

socket.onclose = function () {
	setStatus('offline')
};

document.forms.loginForm.onsubmit = function loginFormSubmit() {
	if (document.getElementById("userName").value === '') {
		alert("Input your name, please");
	} else {
		clientMessage.client = document.getElementById("userName").value;
		logInName.textContent = 'You log in as: ' + clientMessage.client;
		clientMessage.typeOfData = 'logIn';
		socket.send(JSON.stringify(clientMessage));
		setTimeout(loginForm.remove(),2000);
		return false;
	}
};

document.forms.messageInput.onsubmit = function messageInputOnSubmit() {
	if (messageInput.value) {
		clientMessage.message = messageInput.value;
		if (clientMessage.typeOfData === 'logOut') {
			clearClientList()
		}
		clientMessage.typeOfData = 'message';
		socket.send(JSON.stringify(clientMessage));
		// console.log(clientMessage);
		messageInput.value = '';
		return false;
	} else return false;
};



function onMessage(message) {
	const serverMessage = JSON.parse(message);
	switch (serverMessage.typeOfData) {
		case 'clients':
			// console.log(serverMessage.data)
			clientCase(serverMessage.data, chatsBuffer);
			activeChat(activeChatId);
			break;
		case 'chats':
			chatsCase(serverMessage.data);
			break;
		case 'message':
			messageCase(serverMessage);
			break;
		case 'history':
			historyCase(serverMessage.data);
	}
}
function clientCase(clientData, chatsBuffer) {
	clearAllChats(chatLists, messageContainers, clientLists);
	let myChats = [];
	for (let index = 0; index < clientData.length; index++) {
		if (clientData[index].client === clientMessage.client) {
			myChats = clientData[index].onChat;
		}
	}
console.log(myChats)
	myChats.forEach((item) => {
		for (let index = 0; index < chatsBuffer.length; index++) {
			if(chatsBuffer[index].chatId === item) {
				createChat(chatsBuffer[index].chatId, chatsBuffer[index].chatName);
				break;
			}
		}
		
	});
	myChats.forEach((chatId) => {
		clientData.forEach((client) => {
			if (client.client !== clientMessage.client) {
				for (let index = 0; index < client.onChat.length; index++) {
					if (chatId === client.onChat[index]) {
						addToClientList(client, clientLists.children[chatId])
					}
				}
			}
		});
	});
}

function chatsCase(chatsData) {
	chatsBuffer = chatsData;
}

function messageCase(messageData) {
	console.log(messageData);
	let messageContainer = messageContainers.children[messageData.chatId];
	messageContainer.append(createMessageBlock(messageData));
}

function historyCase(historyData) {
	historyData.forEach((historyItem) => {
		const index = historyItem.chatId;
		if (messageContainers.children[index]) {
			const messageContainer = messageContainers.children[index];
			historyItem.data.forEach((item) => {
				messageContainer.append(createMessageBlock(item))
			})
		}
	})
}

function changeChat(input) {
	clientMessage.chatId = input.value;
	for (let index = 0; index < messageContainers.children.length; index++) {
		if (input.id === messageContainers.children[index].id) {
			messageContainers.children[index].style.display = '';
		} else {
			messageContainers.children[index].style.display = 'none';
		}
	}
	for (let index = 0; index < clientLists.children.length; index++) {
		if (input.id === clientLists.children[index].id) {
			clientLists.children[index].style.display = '';
		} else {
			clientLists.children[index].style.display = 'none';
		}
	}
}

function activeChat(chatId) {
	for (let index = 0; index < chooseChat.length; index++) {
		if (chatId == chooseChat[index].value) {
			activeChatId = index;
			chooseChat[index].checked = true;
			changeChat(chooseChat[index]);
		}
	}
}

function createMessageBlock(message) {
		const messageElement = document.createElement('span');
		const userElement = document.createElement('span');
		// const editButton = document.createElement('span');
		// const deleteButton = document.createElement('span');
		const messageBlock = document.createElement('div');
		messageBlock.classList.add('chat');
		messageBlock.classList.add('messageBlock');
		userElement.classList.add('messageBlock');
		userElement.classList.add('messageBlock__user');
		userElement.append(document.createTextNode(message.client + ':'));
		messageElement.classList.add('messageBlock');
		messageElement.classList.add('messageBlock__message');
		// editButton.classList.add('messageBlock');
		// editButton.classList.add('messageBlock__editButton');
		// editButton.innerText = 'edit';
		// deleteButton.classList.add('messageBlock');
		// deleteButton.classList.add('messageBlock__deleteButton');
		// deleteButton.innerText = 'x';
		messageElement.append(document.createTextNode(message.message));
		messageBlock.append(userElement);
		messageBlock.append(messageElement);
		// messageBlock.append(editButton);
		// messageBlock.append(deleteButton);
		return messageBlock;
}

function addToClientList(inputClientData, clientList) {
	const client = document.createElement('p');
	const clientName = document.createElement('span');
	const clientStatus = document.createElement('span');
	clientName.innerText = `${inputClientData.client}`;
	clientStatus.innerText = `${inputClientData.status}`;
	clientName.classList.add('clientListContainer__text');
	clientName.classList.add('clientListContainer__text-userName');
	client.appendChild(clientName);
	client.appendChild(clientStatus);
	client.classList.add('clientListContainer__clientList');
	clientList.append(client);
	return clientList;
}

function setStatus(value) {
	if (value === 'online') {
		status.innerText = value;
	} else {
		status.innerText = 'offline';
	}
}

function clearAllChats(chatLists, messageContainers, clientLists) {
	clearChatList(chatLists);
	clearMessageContainer(messageContainers);
	clearClientList(clientLists);
}

function clearClientList(clientLists) {
	while(clientLists.lastChild) {
		clientLists.lastChild.remove();
	}
}

function clearChatList(chatLists) {
	while(chatLists.lastChild) {
		chatLists.lastChild.remove();
	}
}

function clearMessageContainer(messageContainers) {
	while(messageContainers.lastChild) {
		messageContainers.lastChild.remove();
	}
}

function createChat(inputChatId, inputChatName) {
	let chatName;
	let chatId;
	if (inputChatName) {
		chatId = inputChatId;
		chatName = inputChatName;
	} else {
		chatName = createChatForm.value;
		chatId = chatLists.children.length;
	}
	if (chatName) {
		clientMessage.typeOfData = 'createChat';
		clientMessage.chatId = chatId;
		clientMessage.message = chatName;
		chatLists.append(createChatInChatList(chatId, chatName));
		messageContainers.append(createChatMessageBlock(chatId));
		clientLists.append(createChatClientList(chatId));
	}
}

function createChatInChatList(chatId, chatName) {
		const chatDiv = document.createElement('div');
		const chatRadioButton = document.createElement('input');
		chatDiv.classList.add('chatList');
		chatDiv.classList.add('chatList__room');
		chatDiv.setAttribute('id', `${chatId}`);
		chatDiv.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
		chatRadioButton.setAttribute('type', 'radio');
		chatRadioButton.setAttribute('name', 'chooseChat');
		chatRadioButton.setAttribute('value', `${chatId}`);
		chatRadioButton.setAttribute('id', `${chatId}`);
		chatRadioButton.setAttribute('isChecked', `true`);
		chatDiv.innerText = `${chatName}`;
		chatDiv.prepend(chatRadioButton);
		return chatDiv;
}

function createChatMessageBlock(chatId) {
	const messageContainer = document.createElement('div');
	messageContainer.setAttribute('id', `${chatId}`)
	messageContainer.classList.add('chat');
	messageContainer.style.display = 'none';
	return messageContainer
}

function createChatClientList(chatId) {
	const chatClientList = document.createElement('div');
	chatClientList.setAttribute('id', `${chatId}`)
	chatClientList.classList.add('clientListContainer');
	chatClientList.classList.add('clientListContainer__clientList');
	chatClientList.style.display = 'none';
	return chatClientList;
}

function logOut() {
	clientMessage.typeOfData = 'logOut';
	socket.send(JSON.stringify(clientMessage));
	clearAllChats();
}

