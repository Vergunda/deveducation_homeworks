describe('Chat browser tests', () => {
    
    describe('historyCase function test', () => {
        beforeEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        
        afterEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        it('should display history in correct messageBlock', () => {
            const historyData = [
                {
                    chatId: 0,
                    data: [
                        {chatId: 0, client: 'fdsf', message: 'fdsf join to chat!'},
                        {chatId: 0, client: 'fdsf', message: 'fsdf'},
                        {chatId: 0, client: 'fdsf', message: 'fsdfsd'}
                    ]
                },
                {
                    chatId: 1,
                    data: [
                        {chatId: 1, client: 'fdsf', message: 'fdsf join to chat!'},
                        {chatId: 1, client: 'fdsf', message: 'fsdf'},
                        {chatId: 1, client: 'fdsf', message: 'fsdfsd'}
                    ]
                },
                {
                    chatId: 2,
                    data: [
                        {chatId: 2, client: 'fdsf', message: 'fdsf join to chat!'},
                        {chatId: 2, client: 'fdsf', message: 'fsdf'},
                        {chatId: 2, client: 'fdsf', message: 'fsdfsd'}
                    ]
                },
                {
                    chatId: 3,
                    data: [
                        {chatId: 3, client: 'fdsf', message: 'fdsf join to chat!'},
                        {chatId: 3, client: 'fdsf', message: 'fsdf'},
                        {chatId: 3, client: 'fdsf', message: 'fsdfsd'}
                    ]
                },
            ];
            chatsBuffer = [
                {
                    chatId: 0,
                    chatName: 'main chat',
                },
                {
                    chatId: 1,
                    chatName: 'first chat',
                },
                {
                    chatId: 2,
                    chatName: 'second chat',
                },
                {
                    chatId: 3,
                    chatName: 'third chat',
                },
            ];
            createChat(chatsBuffer[0].chatId, chatsBuffer[0].chatName);
            createChat(chatsBuffer[1].chatId, chatsBuffer[1].chatName);
            createChat(chatsBuffer[2].chatId, chatsBuffer[2].chatName);
            createChat(chatsBuffer[3].chatId, chatsBuffer[3].chatName);
            historyData.forEach((item) => {
                let messageContainer;
                for (let index = 0; index < messageContainers.children.length; index++) {
                    if (item.chatId == messageContainers.children[index].id) {
                        messageContainer = messageContainers.children[index];
                    }
                }
                for (let index = 0; index < item.data.length; index++) {
                    messageContainer.append(createMessageBlock(item.data[index]))
                }
            });
            const expectedMessageContainer0 = messageContainers.children[0];
            const expectedMessageContainer1 = messageContainers.children[1];
            const expectedMessageContainer2 = messageContainers.children[2];
            const expectedMessageContainer3 = messageContainers.children[3];
            
            historyCase(historyData);
            
            assert.deepEqual(expectedMessageContainer0, messageContainers.children[0]);
            assert.deepEqual(expectedMessageContainer1, messageContainers.children[1]);
            assert.deepEqual(expectedMessageContainer2, messageContainers.children[2]);
            assert.deepEqual(expectedMessageContainer3, messageContainers.children[3]);
        });
    });
 
    describe('createChatInChatList function test', () => {
        
        it('Should create new block chat in chatList', () => {
            const chatId = 0;
            const chatName = 'mainChat';
            const chatDiv = document.createElement('div');
            const chatRadioButton = document.createElement('input');
            chatDiv.classList.add('chatList');
            chatDiv.classList.add('chatList__room');
            chatDiv.setAttribute('id', `${chatId}`);
            chatDiv.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton.setAttribute('type', 'radio');
            chatRadioButton.setAttribute('name', 'chooseChat');
            chatRadioButton.setAttribute('value', `${chatId}`);
            chatRadioButton.setAttribute('id', `${chatId}`);
            chatRadioButton.setAttribute('isChecked', `true`);
            chatDiv.innerText = `${chatName}`;
            chatDiv.prepend(chatRadioButton);
            
            const actual = createChatInChatList(chatId, chatName);
            
            assert.deepEqual(chatDiv.outerHTML, actual.outerHTML);
        });
        
        it('Should create new block chat in chatList', () => {
            const chatId = 1;
            const chatName = 'firstChat';
            const chatDiv = document.createElement('div');
            const chatRadioButton = document.createElement('input');
            chatDiv.classList.add('chatList');
            chatDiv.classList.add('chatList__room');
            chatDiv.setAttribute('id', `${chatId}`);
            chatDiv.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton.setAttribute('type', 'radio');
            chatRadioButton.setAttribute('name', 'chooseChat');
            chatRadioButton.setAttribute('value', `${chatId}`);
            chatRadioButton.setAttribute('id', `${chatId}`);
            chatRadioButton.setAttribute('isChecked', `true`);
            chatDiv.innerText = `${chatName}`;
            chatDiv.prepend(chatRadioButton);
            
            const actual = createChatInChatList(chatId, chatName);
            
            assert.deepEqual(chatDiv.outerHTML, actual.outerHTML);
        })
    });
    
    describe('CreateMessageBlock function test', () => {
        
        it('Should create and return message block', () => {
            const chatId = 0;
            const messageContainer = document.createElement('div');
            messageContainer.setAttribute('id', `${chatId}`)
            messageContainer.classList.add('chat');
            messageContainer.style.display = 'none';
            
            const actual = createChatMessageBlock(chatId);
            
            assert.deepEqual(messageContainer, actual);
        });
        
        it('Should create and return message block', () => {
            const chatId = 1;
            const messageContainer = document.createElement('div');
            messageContainer.setAttribute('id', `${chatId}`)
            messageContainer.classList.add('chat');
            messageContainer.style.display = 'none';
            
            const actual = createChatMessageBlock(chatId);
            
            assert.deepEqual(messageContainer, actual);
        });
    });
    
    describe('createChatClientList function test', () => {
        it('Should create and return clientList for chat', () => {
            const chatId = 0;
            const chatClientList = document.createElement('div');
            chatClientList.setAttribute('id', `${chatId}`)
            chatClientList.classList.add('clientListContainer');
            chatClientList.classList.add('clientListContainer__clientList');
            chatClientList.style.display = 'none';
            
            const actual = createChatClientList(chatId)
            
            assert.deepEqual(chatClientList, actual);
        })
        
        it('Should create and return clientList for chat', () => {
            const chatId = 1;
            const chatClientList = document.createElement('div');
            chatClientList.setAttribute('id', `${chatId}`)
            chatClientList.classList.add('clientListContainer');
            chatClientList.classList.add('clientListContainer__clientList');
            chatClientList.style.display = 'none';
            
            const actual = createChatClientList(chatId)
            
            assert.deepEqual(chatClientList, actual);
        })
    });
    
    describe('clearClientList function tests', () => {
        
        it(`Should clear client list`, () => {
            const clientData = [];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientLists.appendChild(client);
            });
            const actual = '';
            
            clearClientList(clientLists);
            
            
            assert.strictEqual(clientLists.innerHTML, actual);
        });
        it(`Should clear client list with 2 elements`, () => {
            const clientData = [{"user": "2", "id": 548953, "status": "online"}, {
                "user": "Dima",
                "id": 541728,
                "status": "online"
            }];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientLists.appendChild(client);
            });
            const actual = '';
            
            clearClientList(clientLists);
            
            
            assert.strictEqual(clientLists.innerHTML, actual);
        });
        it(`Should clear client list with 1 element`, () => {
            const clientData = [{"user": "2", "id": 548953, "status": "online"}];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientLists.appendChild(client);
            });
            const actual = '';
            
            clearClientList(clientLists);
            
            
            assert.strictEqual(clientLists.innerHTML, actual);
        });
    });
    
    describe('clearChatList function tests', () => {
        
        it(`Should clear chat list`, () => {
            const clientData = [];
            clientData.forEach((item) => {
                chatLists.append(createChat(item.chatId, item.chatName));
            });
            
            clearChatList(chatLists);
            
            assert.strictEqual(chatLists.innerHTML, '');
        });
        it(`Should clear chat list with 3 elements`, () => {
            const clientData = [{chatId: 0, chatName: "firstChat"}, {chatId: 1, chatName: "secondChat"}, {
                chatId: 2,
                chatName: "thirdChat"
            }];
            clientData.forEach((item) => {
                chatLists.append(createChat(item.chatId, item.chatName));
            });
            
            clearChatList(chatLists);
            
            assert.strictEqual(chatLists.innerHTML, '');
        });
        it(`Should clear chat list with 1 element`, () => {
            const clientData = [{chatId: 0, chatName: "firstChat"}];
            clientData.forEach((item) => {
                chatLists.append(createChat(item.chatId, item.chatName));
            });
            
            clearChatList(chatLists);
            
            assert.strictEqual(chatLists.innerHTML, '');
        });
    });
    
    describe('clearMessageContainer function tests', () => {
        
        it(`Should clear messageContainer list`, () => {
            
            clearMessageContainer(messageContainers);
            
            assert.strictEqual(messageContainers.innerHTML, '');
        });
        it(`Should clear message containers list with 3 elements`, () => {
            const clientData = [0, 1, 2];
            clientData.forEach((item) => {
                messageContainers.append(createChatMessageBlock(item));
            });
            
            clearMessageContainer(messageContainers);
            
            assert.strictEqual(messageContainers.innerHTML, '');
        });
        it(`Should clear message containers list with 1 element`, () => {
            const clientData = [0];
            clientData.forEach((item) => {
                messageContainers.append(createChatMessageBlock(item));
            });
            
            clearMessageContainer(messageContainers)
            
            assert.strictEqual(messageContainers.innerHTML, '');
        });
    });
    
    describe('clearAllChats function test', () => {
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
        it('Should call clear functions with parameters', () => {
            const spyChat = sandbox.spy(window, 'clearChatList');
            const spyMessageContainer = sandbox.spy(window, 'clearMessageContainer');
            const spyClientList = sandbox.spy(window, 'clearClientList');
            
            clearAllChats(chatLists, messageContainers, clientLists);
            
            assert(spyChat.withArgs(chatLists).calledOnce);
            assert(spyMessageContainer.withArgs(messageContainers).calledOnce);
            assert(spyClientList.withArgs(clientLists).calledOnce);
        })
    });
    
    describe('setStatus function tests', () => {
        
        it(`Should change status on online if value = online`, () => {
            const expectedStatus = 'online';
            const value = 'online';
            
            setStatus(value);
            const actual = status.innerText;
            
            assert.strictEqual(expectedStatus, actual);
        });
        it(`Should change status on offline if value = offline`, () => {
            const expectedStatus = 'offline';
            const value = 'offline';
            
            setStatus(value);
            const actual = status.innerText;
            
            assert.strictEqual(expectedStatus, actual);
        });
        it(`Should change status on offline if value = something`, () => {
            const expectedStatus = 'offline';
            const value = 'offline';
            
            setStatus(value);
            const actual = status.innerText;
            
            assert.strictEqual(expectedStatus, actual);
        });
    });
    
    describe('createChat function test', () => {
        it('should create chat in chatlist, message container and client list for new chat', () => {
            const chatId = 1;
            const chatName = 'First chat';
            const chatInChatList = createChatInChatList(chatId, chatName);
            const messageContainer = createChatMessageBlock(chatId);
            const chatClientList = createChatClientList(chatId);
            const expectedClientMessage = {};
            expectedClientMessage.typeOfData = 'createChat';
            expectedClientMessage.chat = chatId;
            expectedClientMessage.message = chatName;
            
            createChat(chatId, chatName);
            
            assert.deepEqual(chatLists.lastElementChild.outerHTML, chatInChatList.outerHTML);
            assert.deepEqual(messageContainers.lastElementChild.outerHTML, messageContainer.outerHTML);
            assert.deepEqual(clientLists.lastElementChild.outerHTML, chatClientList.outerHTML);
            assert.strictEqual(expectedClientMessage.typeOfData, clientMessage.typeOfData);
            assert.strictEqual(expectedClientMessage.chat, clientMessage.chatId);
            assert.strictEqual(expectedClientMessage.message, clientMessage.message);
        });
        
        it('should create chat in chatlist, message container and client list for new chat', () => {
            const chatId = 1;
            const chatName = 'First chat';
            const chatInChatList = createChatInChatList(chatId, chatName);
            const messageContainer = createChatMessageBlock(chatId);
            const chatClientList = createChatClientList(chatId);
            const expectedClientMessage = {};
            expectedClientMessage.typeOfData = 'createChat';
            expectedClientMessage.chat = chatId;
            expectedClientMessage.message = chatName;
            
            createChat();
            
            assert.deepEqual(chatLists.lastElementChild.outerHTML, chatInChatList.outerHTML);
            assert.deepEqual(messageContainers.lastElementChild.outerHTML, messageContainer.outerHTML);
            assert.deepEqual(clientLists.lastElementChild.outerHTML, chatClientList.outerHTML);
            assert.strictEqual(expectedClientMessage.typeOfData, clientMessage.typeOfData);
            assert.strictEqual(expectedClientMessage.chat, clientMessage.chatId);
            assert.strictEqual(expectedClientMessage.message, clientMessage.message);
        });
        
        it('should create chat in chatlist, message container and client list for new chat', () => {
            createChatForm.value = '';
            const expectedChatList = chatLists.lastElementChild;
            const expectedMessageContainer = messageContainers.lastElementChild;
            const expectedClientList = clientLists.lastElementChild;
            
            createChat();
            
            assert.deepEqual(chatLists.lastElementChild, expectedChatList);
            assert.deepEqual(messageContainers.lastElementChild, expectedMessageContainer);
            assert.deepEqual(clientLists.lastElementChild, expectedClientList);
        });
        
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        after(() => {
            sandbox.restore();
        });
        it('Should call createChatInChatList, createChatMessageBlock, createChatClientList once', () => {
            const chatId = 1;
            const chatName = 'First chat';
            const spyChatList = sandbox.spy(window, 'createChatInChatList');
            const spyMessageContainer = sandbox.spy(window, 'createChatMessageBlock');
            const spyChatClient = sandbox.spy(window, 'createChatClientList');
            
            createChat(chatId, chatName);
            
            assert(spyChatList.withArgs(chatId, chatName).calledOnce);
            assert(spyMessageContainer.withArgs(chatId).calledOnce);
            assert(spyChatClient.withArgs(chatId).calledOnce);
        })
    });
    
    describe('activeChat function test', () => {
        let sandbox = null;
        
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
        
        it('Should call activeChat function once with chatId = "firstChat"', () => {
            clearChatList(chatLists);
            let chatId = 0;
            let chatName = 'firstChat';
            const firstChat = document.createElement('div');
            const chatRadioButton = document.createElement('input');
            firstChat.classList.add('chatList');
            firstChat.classList.add('chatList__room');
            firstChat.setAttribute('id', `${chatId}`);
            firstChat.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton.setAttribute('type', 'radio');
            chatRadioButton.setAttribute('name', 'chooseChat');
            chatRadioButton.setAttribute('value', `${chatId}`);
            chatRadioButton.setAttribute('id', `${chatId}`);
            chatRadioButton.setAttribute('isChecked', `true`);
            firstChat.innerText = `${chatName}`;
            firstChat.prepend(chatRadioButton);
            chatLists.append(firstChat);
            chatId = 1;
            chatName = 'secondChat';
            const secondChat = document.createElement('div');
            const chatRadioButton2 = document.createElement('input');
            secondChat.classList.add('chatList');
            secondChat.classList.add('chatList__room');
            secondChat.setAttribute('id', `${chatId}`);
            secondChat.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton2.setAttribute('type', 'radio');
            chatRadioButton2.setAttribute('name', 'chooseChat');
            chatRadioButton2.setAttribute('isChecked', `true`);
            chatRadioButton2.setAttribute('value', `${chatId}`);
            chatRadioButton2.setAttribute('id', `${chatId}`);
            secondChat.innerText = `${chatName}`;
            secondChat.prepend(chatRadioButton2);
            chatLists.append(secondChat);
            
            const spy = sandbox.spy(window, 'changeChat');
            
            activeChat(0);
            
            assert(spy.withArgs(chooseChat[0]).calledOnce);
            assert.isTrue(chooseChat[0].checked);
            assert.isFalse(chooseChat[1].checked)
        });
        it('Should call activeChat function once with chatId = "firstChat"', () => {
            clearChatList(chatLists);
            let chatId = 0;
            let chatName = 'firstChat';
            const firstChat = document.createElement('div');
            const chatRadioButton = document.createElement('input');
            firstChat.classList.add('chatList');
            firstChat.classList.add('chatList__room');
            firstChat.setAttribute('id', `${chatId}`);
            firstChat.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton.setAttribute('type', 'radio');
            chatRadioButton.setAttribute('name', 'chooseChat');
            chatRadioButton.setAttribute('value', `${chatId}`);
            chatRadioButton.setAttribute('id', `${chatId}`);
            chatRadioButton.setAttribute('isChecked', `true`);
            firstChat.innerText = `${chatName}`;
            firstChat.prepend(chatRadioButton);
            chatLists.append(firstChat);
            chatId = 1;
            chatName = 'secondChat';
            const secondChat = document.createElement('div');
            const chatRadioButton2 = document.createElement('input');
            secondChat.classList.add('chatList');
            secondChat.classList.add('chatList__room');
            secondChat.setAttribute('id', `${chatId}`);
            secondChat.setAttribute('onclick', `activeChat(\`${chatId}\`)`);
            chatRadioButton2.setAttribute('type', 'radio');
            chatRadioButton2.setAttribute('name', 'chooseChat');
            chatRadioButton2.setAttribute('isChecked', `true`);
            chatRadioButton2.setAttribute('value', `${chatId}`);
            chatRadioButton2.setAttribute('id', `${chatId}`);
            secondChat.innerText = `${chatName}`;
            secondChat.prepend(chatRadioButton2);
            chatLists.append(secondChat);
            
            const spy = sandbox.spy(window, 'changeChat');
            
            activeChat(1);
            
            assert(spy.withArgs(chooseChat[1]).calledOnce);
            assert.isTrue(chooseChat[1].checked);
            assert.isFalse(chooseChat[0].checked);
        });
        
    });
    
    describe('changeChat function test', () => {
        beforeEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        afterEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        it('Should display only the current messageContainer and clientList', () => {
            createChat(0, 'firstChat');
            createChat(1, 'secondChat');
            createChat(2, 'thirdChat');
            
            changeChat(chooseChat[0]);
            
            assert.strictEqual(clientMessage.chatId, chooseChat[0].value);
            assert.strictEqual(messageContainers.children[0].style.display, '');
            assert.strictEqual(clientLists.children[0].style.display, '');
            assert.strictEqual(messageContainers.children[1].style.display, 'none');
            assert.strictEqual(messageContainers.children[2].style.display, 'none');
            assert.strictEqual(clientLists.children[1].style.display, 'none');
            assert.strictEqual(clientLists.children[2].style.display, 'none')
        });
        it('Should display only the current messageContainer and clientList', () => {
            createChat(0, 'firstChat');
            createChat(1, 'secondChat');
            createChat(2, 'thirdChat');
            
            changeChat(chooseChat[1]);
            
            assert.strictEqual(clientMessage.chatId, chooseChat[1].value);
            assert.strictEqual(messageContainers.children[1].style.display, '');
            assert.strictEqual(clientLists.children[1].style.display, '');
            assert.strictEqual(messageContainers.children[0].style.display, 'none');
            assert.strictEqual(messageContainers.children[2].style.display, 'none');
            assert.strictEqual(clientLists.children[0].style.display, 'none');
            assert.strictEqual(clientLists.children[2].style.display, 'none')
        });
        it('Should display only the current messageContainer and clientList', () => {
            createChat(0, 'firstChat');
            createChat(1, 'secondChat');
            createChat(2, 'thirdChat');
            
            changeChat(chooseChat[2]);
            
            assert.strictEqual(clientMessage.chatId, chooseChat[2].value);
            assert.strictEqual(messageContainers.children[2].style.display, '');
            assert.strictEqual(clientLists.children[2].style.display, '');
            assert.strictEqual(messageContainers.children[1].style.display, 'none');
            assert.strictEqual(messageContainers.children[0].style.display, 'none');
            assert.strictEqual(clientLists.children[1].style.display, 'none');
            assert.strictEqual(clientLists.children[0].style.display, 'none')
        })
    });
    
    describe('addToClientList function test', () => {
        it('should add 1 client to client list with status', () => {
            const inputClientData = {
                client: 'Dima',
                status: 'online',
                onChat: [0, 1, 2, 3]
            };
            const client = document.createElement('p');
            const clientName = document.createElement('span');
            const clientStatus = document.createElement('span');
            const clientList = createChatClientList(0);
            clientName.innerText = `${inputClientData.client}`;
            clientStatus.innerText = `${inputClientData.status}`;
            clientName.classList.add('clientListContainer__text');
            clientName.classList.add('clientListContainer__text-userName');
            client.appendChild(clientName);
            client.appendChild(clientStatus);
            client.classList.add('clientListContainer__clientList');
            clientList.append(client);
            
            const actual = addToClientList(inputClientData, createChatClientList(0))
            
            assert.deepEqual(clientList, actual);
        });
        it('should add 1 client to client list with status', () => {
            const inputClientData = {
                client: 'Hanna',
                status: 'online',
                onChat: [0, 1, 2, 3]
            };
            const client = document.createElement('p');
            const clientName = document.createElement('span');
            const clientStatus = document.createElement('span');
            const clientList = createChatClientList(0);
            clientName.innerText = `${inputClientData.client}`;
            clientStatus.innerText = `${inputClientData.status}`;
            clientName.classList.add('clientListContainer__text');
            clientName.classList.add('clientListContainer__text-userName');
            client.appendChild(clientName);
            client.appendChild(clientStatus);
            client.classList.add('clientListContainer__clientList');
            clientList.append(client);
            
            const actual = addToClientList(inputClientData, createChatClientList(0))
            
            assert.deepEqual(clientList, actual);
        });
    });
    
    describe('onMessage function test', () => {
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
        it('Should call right function to explore', () => {
            const message = JSON.stringify({
                typeOfData: 'clients',
                data: []
            });
            const stub = sandbox.stub(window, 'clientCase');
            
            onMessage(message);
            
            assert(stub.withArgs(JSON.parse(message).data).calledOnce)
        });
        it('Should call right function to explore', () => {
            const message = JSON.stringify({
                typeOfData: 'chats',
                data: [],
            });
            const stub = sandbox.stub(window, 'chatsCase');
            
            onMessage(message);
            
            assert(stub.withArgs(JSON.parse(message).data).calledOnce)
        });
        it('Should call right function to explore', () => {
            const message = JSON.stringify({
                typeOfData: 'message',
                data: [],
            });
            const stub = sandbox.stub(window, 'messageCase');
            
            onMessage(message);
            
            assert(stub.calledOnce)
        });
        it('Should call right function to explore', () => {
            const message = JSON.stringify({
                typeOfData: 'history',
                data: [],
            });
            const stub = sandbox.stub(window, 'historyCase');
            
            onMessage(message);
            
            assert(stub.withArgs(JSON.parse(message).data).calledOnce)
        });
    });
    
    describe('chatsCase function test', () => {
        
        it('Should add all chats to buffer', () => {
            
            const chatsData = [
                {
                    chatId: 0,
                    chatName: 'main chat',
                },
                {
                    chatId: 1,
                    chatName: 'first chat',
                },
                {
                    chatId: 2,
                    chatName: 'second chat',
                },
                {
                    chatId: 3,
                    chatName: 'third chat',
                },
            ];
            
            chatsCase(chatsData);
            
            assert.deepEqual(chatsBuffer, chatsData);
        });
    });
    
    describe('clientCase function test', () => {
        
        before(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        
        afterEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        
        it('Should create subscribed chats add clients to chats client lists', () => {
            const clientData = [
                {
                    client: 'Dima',
                    status: 'online',
                    onChat: [0, 1, 2, 3]
                },
                {
                    client: 'Hanna',
                    status: 'online',
                    onChat: [0, 1, 3]
                },
                {
                    client: 'Cinamonchik',
                    status: 'online',
                    onChat: [0, 3]
                },
            ];
            clientMessage.client = 'Dima';
            
            createChat(chatsBuffer[0].chatId, chatsBuffer[0].chatName);
            createChat(chatsBuffer[1].chatId, chatsBuffer[1].chatName);
            createChat(chatsBuffer[2].chatId, chatsBuffer[2].chatName);
            createChat(chatsBuffer[3].chatId, chatsBuffer[3].chatName);
            addToClientList(clientData[1], clientLists.children[0]);
            addToClientList(clientData[2], clientLists.children[0]);
            addToClientList(clientData[1], clientLists.children[1]);
            addToClientList(clientData[1], clientLists.children[3]);
            addToClientList(clientData[2], clientLists.children[3]);
            const expectedClientList0 = clientLists.children[0];
            const expectedClientList1 = clientLists.children[1];
            const expectedClientList2 = clientLists.children[2];
            const expectedClientList3 = clientLists.children[3];
            clearAllChats(chatLists, messageContainers, clientLists);
            
            clientCase(clientData, chatsBuffer);
    
            assert.deepEqual(clientLists.children[0].outerHTML, expectedClientList0.outerHTML);
            assert.deepEqual(clientLists.children[1].outerHTML, expectedClientList1.outerHTML);
            assert.deepEqual(clientLists.children[2].outerHTML, expectedClientList2.outerHTML);
            assert.deepEqual(clientLists.children[3].outerHTML, expectedClientList3.outerHTML)
        });
        
        it('Should create subscribed chats add clients to chats client lists', () => {
            const clientData = [
                {
                    client: 'Dima',
                    status: 'online',
                    onChat: [0, 1, 2, 3]
                },
                {
                    client: 'Hanna',
                    status: 'online',
                    onChat: [0, 1, 2, 3]
                },
                {
                    client: 'Cinamonchik',
                    status: 'online',
                    onChat: [0, 1, 3]
                },
            ];
            
            clientMessage.client = 'Dima';
            createChat(chatsBuffer[0].chatId, chatsBuffer[0].chatName);
            createChat(chatsBuffer[1].chatId, chatsBuffer[1].chatName);
            createChat(chatsBuffer[2].chatId, chatsBuffer[2].chatName);
            createChat(chatsBuffer[3].chatId, chatsBuffer[3].chatName);
            addToClientList(clientData[1], clientLists.children[0]);
            addToClientList(clientData[1], clientLists.children[2]);
            addToClientList(clientData[2], clientLists.children[0]);
            addToClientList(clientData[1], clientLists.children[1]);
            addToClientList(clientData[1], clientLists.children[3]);
            addToClientList(clientData[2], clientLists.children[1]);
            addToClientList(clientData[2], clientLists.children[3]);
            const expectedClientList0 = clientLists.children[0];
            const expectedClientList1 = clientLists.children[1];
            const expectedClientList2 = clientLists.children[2];
            const expectedClientList3 = clientLists.children[3];
            clearAllChats(chatLists, messageContainers, clientLists);
    
            clientCase(clientData, chatsBuffer);
         
            assert.deepEqual(clientLists.children[0].outerHTML, expectedClientList0.outerHTML);
            assert.deepEqual(clientLists.children[1].outerHTML, expectedClientList1.outerHTML);
            assert.deepEqual(clientLists.children[2].outerHTML, expectedClientList2.outerHTML);
            assert.deepEqual(clientLists.children[3].outerHTML, expectedClientList3.outerHTML)
        })
    });
    
    describe('createMessageBlock function tests', () => {
        
        it(`createMessageBlock should create message block  serverMessage ={chatId: 0, client: 'Dima', message: 'Hi!}`, () => {
            const serverMessage = {
                chatId: 0,
                client: 'Dima',
                message: 'Hi!',
                };
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.client + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
            
            const actual = createMessageBlock(serverMessage);
            
            assert.deepEqual(expectedMessageBlock, actual);
        });
        
        it(`createMessageBlock should create message block  serverMessage ={chatId: 0, client: 'Dima', message: 'Hi!}`, () => {
            const serverMessage = {
                chatId: 1,
                client: 'Dima',
                message: 'Hi!',
            };
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.client + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);

            const actual = createMessageBlock(serverMessage);

            assert.deepEqual(expectedMessageBlock, actual);
        });
    });
    
    describe('messageCase function test',() => {
        beforeEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
    
        afterEach(() => {
            clearAllChats(chatLists, messageContainers, clientLists);
        });
        it ('Should create messageBlock and add it to messageContainer', () => {
            const messageData = {
                chatId: 1,
                client: 'Dima',
                message: 'Hi!',
            };
            createChat(chatsBuffer[0].chatId, chatsBuffer[0].chatName);
            createChat(chatsBuffer[1].chatId, chatsBuffer[1].chatName);
            messageContainers.children[1].append(createMessageBlock(messageData));
            const expectedMessageContainer0 = messageContainers.children[0];
            const expectedMessageContainer1 = messageContainers.children[1];
            
            messageCase(messageData);

            assert.deepEqual(expectedMessageContainer0, messageContainers.children[0]);
            assert.deepEqual(expectedMessageContainer1, messageContainers.children[1]);
        });
        it ('Should create messageBlock and add it to messageContainer', () => {
            const messageData = {
                chatId: 0,
                client: 'Dima',
                message: 'Hi!',
            };
            createChat(chatsBuffer[0].chatId, chatsBuffer[0].chatName);
            createChat(chatsBuffer[1].chatId, chatsBuffer[1].chatName);
            messageContainers.children[1].append(createMessageBlock(messageData));
            const expectedMessageContainer0 = messageContainers.children[0];
            const expectedMessageContainer1 = messageContainers.children[1];
            
            messageCase(messageData);
           
            assert.deepEqual(expectedMessageContainer0, messageContainers.children[0]);
            assert.deepEqual(expectedMessageContainer1, messageContainers.children[1]);
        });
    });
    
});


//
//
//

//
// describe('showMessage function test', () => {
//     let sandbox = null;
//
//     before(() => {
//         sandbox = sinon.createSandbox();
//     });
//
//     afterEach(() => {
//         sandbox.restore();
//     });
//
//     it('Should check messageContainer3000 and append messageBlock', () => {
//         const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}
//         const messageElement = document.createElement('span');
//         const userElement = document.createElement('span');
//         const expectedMessageBlock = document.createElement('div');
//         expectedMessageBlock.classList.add('chat');
//         expectedMessageBlock.classList.add('messageBlock');
//         userElement.classList.add('messageBlock');
//         userElement.classList.add('messageBlock__user');
//         userElement.append(document.createTextNode(serverMessage.user + ':'));
//         messageElement.classList.add('messageBlock');
//         messageElement.classList.add('messageBlock__message');
//         messageElement.append(document.createTextNode(serverMessage.message));
//         expectedMessageBlock.append(userElement);
//         expectedMessageBlock.append(messageElement);
//         const expectedMessageContainer = messageContainer3000;
//         expectedMessageContainer.append(expectedMessageBlock);
//
//         showMessage(serverMessage);
//
//         assert.deepEqual(messageContainer3000,expectedMessageContainer);
//     });
//
//     it('Should call clientList() once', () => {
//         const spy = sandbox.spy(window, 'createMessageBlock');
//         const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}
//
//         showMessage(serverMessage);
//
//         // assert.deepEqual(messageContainer3000,expectedMessageContainer);
//         assert(spy.withArgs(serverMessage).calledOnce);
//     });
// });
//
// describe('logOut function test', () => {
//     let sandbox = null;
//
//     before(() => {
//         sandbox = sinon.createSandbox();
//     });
//
//     afterEach(() => {
//         sandbox.restore();
//     });
//
//     it('Should call for socket3000, socket3001,socket3002 method "send" once to each and clientList() once', () => {
//         const spySocket3000 = sandbox.stub(socket3000, 'send');
//         const spySocket3001 = sandbox.stub(socket3001, 'send');
//         const spySocket3002 = sandbox.stub(socket3002, 'send');
//         const spyClientList = sandbox.stub(window, 'clearClientList');
//         const expectedUserMessage = 'exit';
//
//         logOut();
//         assert(spySocket3000.calledOnce);
//         assert(spySocket3001.calledOnce);
//         assert(spySocket3002.calledOnce);
//         assert(spyClientList.calledOnce);
//         assert.strictEqual(userMessage.message, expectedUserMessage);
//     });
//     it('Should change userMessage.message for "exit" ', () => {
//         const spySocket3000 = sandbox.stub(socket3000, 'send');
//         const spySocket3001 = sandbox.stub(socket3001, 'send');
//         const spySocket3002 = sandbox.stub(socket3002, 'send');
//         const spyClientList = sandbox.stub(window, 'clearClientList');
//         const expectedUserMessage = 'exit';
//
//         logOut();
//
//         assert.strictEqual(userMessage.message, expectedUserMessage);
//     });
// });
//
//
// describe('loginFormSubmit function test', () => {
//     let sandbox = null;
//
//     before(() => {
//         sandbox = sinon.createSandbox();
//     });
//
//     afterEach(() => {
//         sandbox.restore();
//     });
//
//     it('Should call alert with "Input your name, please"', () => {
//         document.getElementById("userName").value = '';
//         const spySocket3000 = sandbox.stub(socket3000, 'send');
//         const spySocket3001 = sandbox.stub(socket3001, 'send');
//         const spySocket3002 = sandbox.stub(socket3002, 'send');
//         const spy = sandbox.spy(loginForm, 'remove');
//         const spyAlert = sandbox.stub(window, 'alert');
//
//         loginFormSubmit();
//
//         assert(spyAlert.withArgs("Input your name, please").calledOnce);
//     });
//
//     it('Should call for socket3000, socket3001,socket3002 method "send", loginForm.remove once and return false', () => {
//         document.getElementById("userName").value = 'some text';
//         const spySocket3000 = sandbox.stub(socket3000, 'send');
//         const spySocket3001 = sandbox.stub(socket3001, 'send');
//         const spySocket3002 = sandbox.stub(socket3002, 'send');
//         const spy = sandbox.spy(loginForm, 'remove');
//
//         const actual = loginFormSubmit();1
//
//         assert(spySocket3000.calledOnce);
//         assert(spySocket3001.calledOnce);
//         assert(spySocket3002.calledOnce);
//         assert(spy.calledOnce);
//         assert.isTrue(!actual);
//     });
//   });
//
// describe('messageInputOnSubmit function test', () => {
//     let sandbox = null;
//
//     before(() => {
//         sandbox = sinon.createSandbox();
//     });
//
//     afterEach(() => {
//         sandbox.restore();
//     });
//
//     it('Should return false, when messageInput.value is empty', () => {
//         messageInput.value = '';
//
//         const actual = messageInputOnSubmit();
//
//         assert.isTrue(!actual);
//     });
//
//     it('Should call socket.send, clear messageInput.value and return false, when messageInput.value = "some text" ', () => {
//         messageInput.value = 'some text';
//         const spySocket = sandbox.stub(socket, 'send');
//
//         const actual = messageInputOnSubmit();
//
//         assert(spySocket.withArgs(JSON.stringify(userMessage)).calledOnce);
//         assert.isTrue(!messageInput.value);
//         assert.isTrue(!actual);
//     });
//     it('Should call socket.send, clear messageInput.value and return false, when messageInput.value = "exit" ', () => {
//         messageInput.value = 'exit';
//         const spySocket = sandbox.stub(socket, 'send');
//         const spyClearClientList = sandbox.spy(window, 'clearClientList');
//
//         const actual = messageInputOnSubmit();
//
//         assert(spySocket.withArgs(JSON.stringify(userMessage)).calledOnce);
//         assert(spyClearClientList.calledOnce)
//         assert.isTrue(!messageInput.value);
//         assert.isTrue(!actual);
//     });
// });
//


function loginFormSubmit() {
	if (document.getElementById("userName").value === '') {
		alert("Input your name, please");
	} else {
		userMessage.user = document.getElementById("userName").value;
		// clientList.appendChild(addUserToChatList(userMessage));
		socket3000.send(JSON.stringify(userMessage));
		socket3001.send(JSON.stringify(userMessage));
		socket3002.send(JSON.stringify(userMessage));
		loginForm.remove();
		return false;
	}
}

function messageInputOnSubmit() {
	if (messageInput.value) {
		userMessage.message = messageInput.value;
		if (userMessage.message === 'exit') {
			clearClientList()
		}
		socket.send(JSON.stringify(userMessage));
		console.log(userMessage);
		messageInput.value = '';
		return false;
	} else return false;
};