let WebSocketServer = new require('ws');

let clients = {};

let chats = {
	typeOfData: 'chats',
	data: [
		{
			chatId: 0,
			chatName: 'main chat',
		},
		{
			chatId: 1,
			chatName: 'secondChat',
		},
		{
			chatId: 2,
			chatName: 'thirdChat',
		},
		{
			chatId: 3,
			chatName: 'forthChat',
		},
	]
};

let clientData = {
	typeOfData: 'clients',
	data: [
		{
			client: 'Dima',
			status:  'offline',
			onChat: [0, 1, 2, 3]
		},
		{
			client: 'Hanna',
			status: 'offline',
			onChat: [0, 1, 2, 3]
		},
		{
			client: 'Korja',
			status:  'offline',
			onChat: [0, 3]
		},
	]
};

// };

let history = {
	typeOfData: 'history',
	data: [
		{
			chatId: 0,
			data: [
				{chatId: 0, client: 'Dima', message: 'Dima join to chat!'},
				{chatId: 0, client: 'Hanna', message: 'firstMessage'},
				{chatId: 0, client: 'Korja', message: 'second'}
			]
		},
		{
			chatId: 1,
			data: [
				{chatId: 1, client: 'someUser', message: 'fdsf join to chat!'},
				{chatId: 1, client: 'fdsf', message: 'fsdf'},
				{chatId: 1, client: 'fdsf', message: 'fsdfsd'}
			]
		},
		{
			chatId: 2,
			data: [
				{chatId: 2, client: 'fdsf', message: 'fdsf join to chat!'},
				{chatId: 2, client: 'fdsf', message: 'fsdf'},
				{chatId: 2, client: 'fdsf', message: 'fsdfsd'}
			]
		},
		{
			chatId: 3,
			data: [
				{chatId: 3, client: 'fdsf', message: 'fdsf join to chat!'},
				{chatId: 3, client: 'fdsf', message: 'fsdf'},
				{chatId: 3, client: 'fdsf', message: 'fsdfsd'}
			]
		},
		]
	
};
// messageContainer.messages = [{
// 	chat: 1, user: 'Server',
// 	message: 'Welcome to chat!'
// }];

let webSocketServer = new WebSocketServer.Server({port: 3000});

webSocketServer.on('connection', function (ws) {
	
	let id = Math.floor(Math.random() * 1000000);
	clients[id] = ws;
	
	ws.send(JSON.stringify(chats));
	
	ws.on('message', function (message) {
		let msg = JSON.parse(message);
		switch (msg.typeOfData) {
			case 'logIn':
				console.log(msg);
				logIn(msg, id, ws);
				clientData.data.find((item) => item.ws === ws).status = 'online';
				for (let key in clients) {
					if (clients[key].readyState === WebSocketServer.OPEN) {
						clients[key].send(JSON.stringify(clientData));
						clients[key].send(JSON.stringify(history));
					}
				}
				break;
			case 'logOut':
				
				clientData.data.find((item) => item.client === msg.client).status = 'offline';
				
				for (let key in clients) {
					if (clients[key].readyState === WebSocketServer.OPEN) {
						clients[key].send(JSON.stringify(clientData));
						clients[key].send(JSON.stringify(history));
					}
				}
				ws.close();
				break;
			case 'message':
				onMessageFunction(msg);
				break;
			case 'createChat':
				console.log(message);
				break;
			case 'addClientToChat':
				break;
			case 'editMessage':
				break;
			case 'deleteMessage':
				break;
			case 'deleteRoom':
				break;
		}
	});
	
	ws.on('close', function () {
		clientData.data.find((item) => item.ws === ws).status = 'offline';
		
		for (let key in clients) {
			if (clients[key].readyState === WebSocketServer.OPEN) {
				clients[key].send(JSON.stringify(clientData));
				clients[key].send(JSON.stringify(history));
			}
		}
		ws.close();
	});
});

function logOutClient(name, clientData) {
	return  clientData.data.find((item) => item.clientName === name)
}
function logIn(clientMessage, id, ws) {
	let checkUniqName;
	if (clientData.data) {
		checkUniqName = clientData.data.find((item) => item.client === clientMessage.client)
	}

	if (checkUniqName) {
		checkUniqName.ws = ws;
	} else {
		const clientDataItem = {};
		clientDataItem.client = clientMessage.client;
		clientDataItem.id = id;
		clientDataItem.status = 'online';
		clientDataItem.onChat = [0];
		clientDataItem.ws = ws;
		clientData.data.push(clientDataItem);
	}

	ws.send(JSON.stringify(clientData));
	console.log(clientData);
	ws.send(JSON.stringify(history));
}

function onMessageFunction(msg) {
	console.log(msg.chatId);

	history.data[msg.chatId].data.push(msg);
	console.log(history.data[0]);
	for (let key in clients) {
		if (clients[key].readyState === WebSocketServer.OPEN) {
			clients[key].send(JSON.stringify(msg));
			console.log(JSON.stringify(msg));
		}
	}
}

function logOut(clientMessage) {

}

function createChat(clientMessage) {

}

function addClientToChat(clientMessage) {

}

// function clientDataForUser(clientData) {
// 	const clientDataForUser = [];
// 	clientData.forEach((item) => {
// 		clientDataForUser = []
// 	})
// }

// function createRequestMessage(message) {
// 	const request = JSON.parse(message);
// 	return JSON.stringify(request);
// }

// function checkUniqName(name, clientData) {
// 	for (let index = 0; index < clientData.length; index++) {
// 		if (name === clientData.name) {
// 			return false;
// 		}
// 	}
// 	return true
// }
//
// function defineChatSubscribers(chatId, clientListData) {
// 	const subscribers = [];
// 	clientListData.forEach((item) => {
// 		if(chatId === item.clieantChats.)
// 	}),
// 	return subscribers;
// }









