let socket3000 = new WebSocket('ws://localhost:3000');
let socket3001 = new WebSocket('ws://localhost:3001');
let socket3002 = new WebSocket('ws://localhost:3002');
let socket = socket3000;
let userMessage = {};
const messageContainer3000 = document.getElementById('chat3000');
const messageContainer3001 = document.getElementById('chat3001');
const messageContainer3002 = document.getElementById('chat3002')
const clientList = document.getElementById('clientList');
const loginForm = document.getElementById("loginForm");
const form = document.getElementById("form");
const messageInput = document.getElementById('message');
const status = document.getElementById('status');
const chooseChat = document.getElementsByName('chooseChat');

socket3000.onmessage = function (event) {
	showMessage(JSON.parse(event.data));
	addToClientList(JSON.parse(event.data));
};

socket3000.onopen = function () {
	setStatus('online');
};

socket3000.onclose = function () {
	setStatus('offline')
};

socket3001.onmessage = function (event) {
	showMessage(JSON.parse(event.data));
	addToClientList(JSON.parse(event.data));
};

socket3001.onopen = function () {
	setStatus('online');
};

socket3001.onclose = function () {
	setStatus('offline')
};

socket3002.onmessage = function (event) {
	showMessage(JSON.parse(event.data));
	addToClientList(JSON.parse(event.data));
};

socket3002.onopen = function () {
	setStatus('online');
};

socket3002.onclose = function () {
	setStatus('offline')
};

document.forms.loginForm.onsubmit = function loginFormSubmit() {
	if (document.getElementById("userName").value === '') {
		alert("Input your name, please");
	} else {
		userMessage.user = document.getElementById("userName").value;
		socket3000.send(JSON.stringify(userMessage));
		socket3001.send(JSON.stringify(userMessage));
		socket3002.send(JSON.stringify(userMessage));
		loginForm.remove();
		return false;
	}
	
};

document.forms.messageInput.onsubmit = function messageInputOnSubmit() {
	if (messageInput.value) {
		userMessage.message = messageInput.value;
		if (userMessage.message === 'exit') {
			clearClientList()
		}
		socket.send(JSON.stringify(userMessage));
		console.log(userMessage);
		messageInput.value = '';
		return false;
	} else return false;
};

function changeChat(input) {
	if (input.value === "firstChat") {
		messageContainer3000.style.display = '';
		messageContainer3001.style.display = 'none';
		messageContainer3002.style.display = 'none';
		socket = socket3000;
	} else if (input.value === "secondChat") {
		messageContainer3000.style.display = 'none';
		messageContainer3001.style.display = '';
		messageContainer3002.style.display = 'none';
		socket = socket3001;
	} else if (input.value === "thirdChat") {
		messageContainer3000.style.display = 'none';
		console.log(messageContainer3002.style.display)
		messageContainer3001.style.display = 'none';
		messageContainer3002.style.display = '';
		console.log(messageContainer3002.style.display)
		socket = socket3002;
	}
}

function activeChat(chatId) {
	for (let index = 0; index < chooseChat.length; index++) {
		if (chatId === chooseChat[index].value) {
			chooseChat[index].checked = true;
			changeChat(chooseChat[index]);
		}
	}
}

function showMessage(serverMessage) {
	let messageContainer;
	switch (serverMessage.server) {
		case 3000:
			messageContainer = messageContainer3000;
			break;
		case 3001:
			messageContainer = messageContainer3001;
			break;
		case 3002:
			messageContainer = messageContainer3002;
			break;
	}
	messageContainer.append(createMessageBlock(serverMessage));
}

function createMessageBlock(serverMessage) {
	const messageElement = document.createElement('span');
	const userElement = document.createElement('span');
	const messageBlock = document.createElement('div');
	messageBlock.classList.add('chat');
	messageBlock.classList.add('messageBlock');
	userElement.classList.add('messageBlock');
	userElement.classList.add('messageBlock__user');
	userElement.append(document.createTextNode(serverMessage.user + ':'));
	messageElement.classList.add('messageBlock');
	messageElement.classList.add('messageBlock__message');
	messageElement.append(document.createTextNode(serverMessage.message));
	messageBlock.append(userElement);
	messageBlock.append(messageElement);
	return messageBlock;
}

function addToClientList(serverMessage) {
	if (serverMessage.clientData) {
		clearClientList();
		const clientData = serverMessage.clientData;
		
		clientData.forEach((item) => {
			const client = document.createElement('p');
			const clientName = document.createElement('span');
			const clientStatus = document.createElement('span');
			clientName.innerText = `${item.user}`;
			clientStatus.innerText = `${item.status}`;
			clientName.classList.add('clientListContainer__text');
			clientName.classList.add('clientListContainer__text-userName');
			client.appendChild(clientName);
			client.appendChild(clientStatus);
			client.classList.add('clientListContainer__clientList');
			clientList.appendChild(client);
		})
	}
}

function setStatus(value) {
	if (value === 'online') {
		status.innerText = value;
	} else {
		status.innerText = 'offline';
	}
	
}

function clearClientList() {
		const amountOfClients = clientList.childNodes.length;
		for (let index = 0; index < amountOfClients; index++) {
			clientList.lastChild.remove();
		}
}

function logOut() {
	userMessage.message = 'exit';
	socket3000.send(JSON.stringify(userMessage));
	socket3001.send(JSON.stringify(userMessage));
	socket3002.send(JSON.stringify(userMessage));
	clearClientList();
}






