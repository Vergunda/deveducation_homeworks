let WebSocketServer = new require('ws');

let clients = {};
let clientData = [];

let webSocketServer = new WebSocketServer.Server({port: 3002});

webSocketServer.on('connection', function (ws) {
    
    let id = Math.floor(Math.random() * 1000000);
    
    clients[id] = ws;
    
    ws.send(createRequestMessage(JSON.stringify({
        user: 'Server',
        message: 'Welcome to first chat!'
    })));
    
    ws.on('close', function () {
        let nameOfLogOutClient = logOutClient(id, clientData);
        for (let key in clients) {
            if (clients[key].readyState === WebSocketServer.OPEN) {
                sendLogOutMessage(clients[key], nameOfLogOutClient);
            }
        }
    });
    
    ws.on('message', function (message) {
        if (!JSON.parse(message).message) {
            const clientDataItem = JSON.parse(message);
            clientDataItem.id = id;
            clientDataItem.status = 'online';
            clientData.push(clientDataItem);
            console.log(clientData);
        } else {
            for (let key in clients) {
                if (JSON.parse(message).message === 'exit') {
                    ws.close();
                } else {
                    if (clients[key].readyState === WebSocketServer.OPEN) {
                        clients[key].send(createRequestMessage(message));
                        console.log(createRequestMessage(message));
                    }
                }
            }
        }
    });
});

function logOutClient(id, clientData) {
    let nameOfLogOutClient;
    for (let index = 0; index < clientData.length; index++) {
        if (id === clientData[index].id) {
            nameOfLogOutClient = clientData[index].user;
            clientData.splice(index, 1);
            return nameOfLogOutClient;
        }
    }
}

function sendLogOutMessage(client, nameOfLogOutClient) {
    client.send(createRequestMessage(JSON.stringify({
        user: 'Server',
        message: `${nameOfLogOutClient} leaved this chat`
    })));
    console.log(createRequestMessage(JSON.stringify({
        user: 'Server',
        message: `${nameOfLogOutClient} leaved this chat`
    })));
}

function createRequestMessage(message) {
    const request = JSON.parse(message);
    request.clientData = clientData;
    request.server = 3002;
    return JSON.stringify(request);
}












