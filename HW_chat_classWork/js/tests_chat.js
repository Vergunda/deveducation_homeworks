describe('Chat browser tests', () => {
    
    describe('changeChat function tests', () => {
        
        it(`changeChat function should make visible second chat when input.value = "thirdChat" and switch socket value for socket3002`, () => {
            const input = chooseChat[2];
            const expectedContainerDisplay = '';
            const expectedHiddenContainerDisplay = 'none';
            const expectedSocket = socket3002;
        
            changeChat(input);
            const containerDisplay = messageContainer3002.style.display;
            const hiddenContainerDisplay1 = messageContainer3000.style.display;
            const hiddenContainerDisplay2 = messageContainer3001.style.display;
            const actualSocket = socket;
        
            assert.deepEqual(expectedSocket, actualSocket);
            assert.strictEqual(expectedContainerDisplay, containerDisplay);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay1);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay2);
        
        });
        it(`changeChat function should make visible third chat when input.value = "secondChat" and switch socket value for socket3001`, () => {
            const input = chooseChat[1];
            const expectedContainerDisplay = '';
            const expectedHiddenContainerDisplay = 'none';
            const expectedSocket = socket3001;
        
            changeChat(input);
            const containerDisplay = messageContainer3001.style.display;
            const hiddenContainerDisplay1 = messageContainer3000.style.display;
            const hiddenContainerDisplay2 = messageContainer3002.style.display;
            const actualSocket = socket;
        
            assert.deepEqual(expectedSocket, actualSocket);
            assert.strictEqual(expectedContainerDisplay, containerDisplay);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay1);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay2);
        });
    
        it(`changeChat function should make visible first chat when input.value = "firstChat" and switch socket value for socket3000`, () => {
            const input = chooseChat[0];
            const expectedContainerDisplay = '';
            const expectedHiddenContainerDisplay = 'none';
            const expectedSocket = socket3000;
        
            changeChat(input);
            
            const containerDisplay = messageContainer3000.style.display;
            const hiddenContainerDisplay1 = messageContainer3002.style.display;
            const hiddenContainerDisplay2 = messageContainer3002.style.display;
            const actualSocket = socket;
            assert.deepEqual(expectedSocket, actualSocket);
            assert.strictEqual(expectedContainerDisplay, containerDisplay);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay1);
            assert.strictEqual(expectedHiddenContainerDisplay, hiddenContainerDisplay2);
        });
    });
    
    describe('createMessageBlock function tests', () => {
        
        it(`createMessageBlock should create message block  serverMessage ={"user":,"message":}`, () => {
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000};
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
            
            
            const actual =  createMessageBlock(serverMessage);
            
            assert.deepEqual(expectedMessageBlock, actual);
        });
        
        it(`createMessageBlock should create message block serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}`, () => {
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
        
        
            const actual =  createMessageBlock(serverMessage);
        
            assert.deepEqual(expectedMessageBlock, actual);
        });
    
        it(`createMessageBlock should create message block, when serverMessage ={"user":"Hanna","message":"Hi!"};`, () => {
            const serverMessage ={"user":"Hanna","message":"Hi!"};
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
        
        
            const actual =  createMessageBlock(serverMessage);
        
            assert.deepEqual(expectedMessageBlock, actual);
        });
        
    });
    
    describe('setStatus function tests', () => {
        
        it(`Should change status on online if value = online`, () => {
            const expectedStatus = 'online';
            const value = 'online';
            
            setStatus(value);
            const actual = status.innerText;
            
            assert.strictEqual(expectedStatus, actual);
        });
        it(`Should change status on offline if value = offline`, () => {
            const expectedStatus = 'offline';
            const value = 'offline';
        
            setStatus(value);
            const actual = status.innerText;
        
            assert.strictEqual(expectedStatus, actual);
        });
        it(`Should change status on offline if value = something`, () => {
            const expectedStatus = 'offline';
            const value = 'offline';
        
            setStatus(value);
            const actual = status.innerText;
        
            assert.strictEqual(expectedStatus, actual);
        });
    });
    
    describe('clearClientList function tests', () => {
        
        it(`Should clear client list`, () => {
            const clientData = [];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientList.appendChild(client);
            });
            const actual = '';
    
            clearClientList(clientList);
    
    
            assert.strictEqual(clientList.innerHTML, actual);
        });
        it(`Should clear client list with 2 elements`, () => {
            const clientData = [{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientList.appendChild(client);
            });
            const actual = '';
            
            clearClientList(clientList);
            
            
            assert.strictEqual(clientList.innerHTML, actual);
        });
        it(`Should clear client list with 1 element`, () => {
            const clientData = [{"user":"2","id":548953,"status":"online"}];
            clientData.forEach((item) => {
                const client = document.createElement('p');
                const clientName = document.createElement('span');
                const clientStatus = document.createElement('span');
                clientName.innerText = `${item.user}`;
                clientStatus.innerText = `${item.status}`;
                clientName.classList.add('clientListContainer__text');
                clientName.classList.add('clientListContainer__text-userName');
                client.appendChild(clientName);
                client.appendChild(clientStatus);
                client.classList.add('clientListContainer__clientList');
                clientList.appendChild(client);
            });
            const actual = '';
    
            clearClientList(clientList);
    
    
            assert.strictEqual(clientList.innerHTML, actual);
        });
    });
    
    describe('showMessage function test', () => {
        let sandbox = null;

        before(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
        });
        
        it('Should check messageContainer3000 and append messageBlock', () => {
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
            const expectedMessageContainer = messageContainer3000;
            expectedMessageContainer.append(expectedMessageBlock);
            
            showMessage(serverMessage);
            
            assert.deepEqual(messageContainer3000,expectedMessageContainer);
        });
        
        it('Should check messageContainer3001 and append messageBlock', () => {
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3001};
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
            const expectedMessageContainer = messageContainer3001;
            expectedMessageContainer.append(expectedMessageBlock);
        
            showMessage(serverMessage);
        
            assert.deepEqual(messageContainer3001,expectedMessageContainer);
        });
    
        it('Should check messageContainer3002 and append messageBlock', () => {
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3002};
            const messageElement = document.createElement('span');
            const userElement = document.createElement('span');
            const expectedMessageBlock = document.createElement('div');
            expectedMessageBlock.classList.add('chat');
            expectedMessageBlock.classList.add('messageBlock');
            userElement.classList.add('messageBlock');
            userElement.classList.add('messageBlock__user');
            userElement.append(document.createTextNode(serverMessage.user + ':'));
            messageElement.classList.add('messageBlock');
            messageElement.classList.add('messageBlock__message');
            messageElement.append(document.createTextNode(serverMessage.message));
            expectedMessageBlock.append(userElement);
            expectedMessageBlock.append(messageElement);
            const expectedMessageContainer = messageContainer3002;
            expectedMessageContainer.append(expectedMessageBlock);
        
            showMessage(serverMessage);
        
            assert.deepEqual(messageContainer3002,expectedMessageContainer);
        });
        
        it('Should call clientList() once', () => {
            const spy = sandbox.spy(window, 'createMessageBlock');
            const serverMessage ={"user":"Dima","message":"Hi!","clientData":[{"user":"2","id":548953,"status":"online"},{"user":"Dima","id":541728,"status":"online"}],"server":3000}
        
            showMessage(serverMessage);
        
            // assert.deepEqual(messageContainer3000,expectedMessageContainer);
            assert(spy.withArgs(serverMessage).calledOnce);
        });
    });

    describe('logOut function test', () => {
        let sandbox = null;

        before(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it('Should call for socket3000, socket3001,socket3002 method "send" once to each and clientList() once', () => {
            const spySocket3000 = sandbox.stub(socket3000, 'send');
            const spySocket3001 = sandbox.stub(socket3001, 'send');
            const spySocket3002 = sandbox.stub(socket3002, 'send');
            const spyClientList = sandbox.stub(window, 'clearClientList');
            const expectedUserMessage = 'exit';

            logOut();
            assert(spySocket3000.calledOnce);
            assert(spySocket3001.calledOnce);
            assert(spySocket3002.calledOnce);
            assert(spyClientList.calledOnce);
            assert.strictEqual(userMessage.message, expectedUserMessage);
        });
        it('Should change userMessage.message for "exit" ', () => {
            const spySocket3000 = sandbox.stub(socket3000, 'send');
            const spySocket3001 = sandbox.stub(socket3001, 'send');
            const spySocket3002 = sandbox.stub(socket3002, 'send');
            const spyClientList = sandbox.stub(window, 'clearClientList');
            const expectedUserMessage = 'exit';
        
            logOut();

            assert.strictEqual(userMessage.message, expectedUserMessage);
        });
    });
    
    describe('activeChat function test', () => {
        let sandbox = null;
        
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
        
        it('Should call changeChat function once with chatId = "firstChat"', () => {
            chooseChat[0].checked = true;
            const chatId = 'firstChat';
            const spy = sandbox.spy(window,'changeChat');
            
            activeChat(chatId);
            
            assert(spy.withArgs(chooseChat[0]).calledOnce);
        });
    
        it('Should call changeChat function once with chatId = "secondChat"', () => {
            chooseChat[1].checked = true;
            const chatId = 'secondChat';
            const spy = sandbox.spy(window,'changeChat');
        
            activeChat(chatId);
        
            assert(spy.withArgs(chooseChat[1]).calledOnce);
        });
        
        it('Should call changeChat function once with chatId = "thirdChat"', () => {
            chooseChat[2].checked = true;
            const chatId = 'thirdChat';
            const spy = sandbox.spy(window,'changeChat');
        
            activeChat(chatId);
        
            assert(spy.withArgs(chooseChat[2]).calledOnce);
        });
    });
    
    describe('loginFormSubmit function test', () => {
        let sandbox = null;
        
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
    
        it('Should call alert with "Input your name, please"', () => {
            document.getElementById("userName").value = '';
            const spySocket3000 = sandbox.stub(socket3000, 'send');
            const spySocket3001 = sandbox.stub(socket3001, 'send');
            const spySocket3002 = sandbox.stub(socket3002, 'send');
            const spy = sandbox.spy(loginForm, 'remove');
            const spyAlert = sandbox.stub(window, 'alert');
        
            loginFormSubmit();
        
            assert(spyAlert.withArgs("Input your name, please").calledOnce);
        });
        
        it('Should call for socket3000, socket3001,socket3002 method "send", loginForm.remove once and return false', () => {
            document.getElementById("userName").value = 'some text';
            const spySocket3000 = sandbox.stub(socket3000, 'send');
            const spySocket3001 = sandbox.stub(socket3001, 'send');
            const spySocket3002 = sandbox.stub(socket3002, 'send');
            const spy = sandbox.spy(loginForm, 'remove');
            
            const actual = loginFormSubmit();
    
            assert(spySocket3000.calledOnce);
            assert(spySocket3001.calledOnce);
            assert(spySocket3002.calledOnce);
            assert(spy.calledOnce);
            assert.isTrue(!actual);
        });
      });
    
    describe('messageInputOnSubmit function test', () => {
        let sandbox = null;
        
        before(() => {
            sandbox = sinon.createSandbox();
        });
        
        afterEach(() => {
            sandbox.restore();
        });
        
        it('Should return false, when messageInput.value is empty', () => {
            messageInput.value = '';
    
            const actual = messageInputOnSubmit();
    
            assert.isTrue(!actual);
        });
        
        it('Should call socket.send, clear messageInput.value and return false, when messageInput.value = "some text" ', () => {
            messageInput.value = 'some text';
            const spySocket = sandbox.stub(socket, 'send');
    
            const actual = messageInputOnSubmit();
            
            assert(spySocket.withArgs(JSON.stringify(userMessage)).calledOnce);
            assert.isTrue(!messageInput.value);
            assert.isTrue(!actual);
        });
        it('Should call socket.send, clear messageInput.value and return false, when messageInput.value = "exit" ', () => {
            messageInput.value = 'exit';
            const spySocket = sandbox.stub(socket, 'send');
            const spyClearClientList = sandbox.spy(window, 'clearClientList');
        
            const actual = messageInputOnSubmit();
        
            assert(spySocket.withArgs(JSON.stringify(userMessage)).calledOnce);
            assert(spyClearClientList.calledOnce)
            assert.isTrue(!messageInput.value);
            assert.isTrue(!actual);
        });
    });
   
});

function loginFormSubmit() {
    if (document.getElementById("userName").value === '') {
        alert("Input your name, please");
    } else {
        userMessage.user = document.getElementById("userName").value;
        // clientList.appendChild(addUserToChatList(userMessage));
        socket3000.send(JSON.stringify(userMessage));
        socket3001.send(JSON.stringify(userMessage));
        socket3002.send(JSON.stringify(userMessage));
        loginForm.remove();
        return false;
    }
}

function messageInputOnSubmit() {
    if (messageInput.value) {
        userMessage.message = messageInput.value;
        if (userMessage.message === 'exit') {
            clearClientList()
        }
        socket.send(JSON.stringify(userMessage));
        console.log(userMessage);
        messageInput.value = '';
        return false;
    } else return false;
};