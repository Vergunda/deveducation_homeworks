let id;
let firstName;
let lastName;
let age;
let dataArray = [];

function getData(dataArray) {
    let isExist = 0;
    const dataObject = {
            id: document.getElementById('id').value,
            firstName: document.getElementById('firstName').value,
            lastName: document.getElementById('lastName').value,
            age: document.getElementById('age').value,
        };
    for (let index = 0; index < dataArray.length; index++) {
        if (dataArray[index].id === dataObject.id) {
            isExist = 1;
        }
    }
    
    if (isExist) {
        return void 0;
    } else {
        dataArray.push(dataObject);
        return dataArray;
    }
    
}

function createCell(data, class1, class2, class3) {
	const tableCell = document.createElement('th');
	tableCell.innerHTML = `${data}`;
	tableCell.classList.add(`${class1}`);
	tableCell.classList.add(`${class2}`);
	tableCell.classList.add(`${class3}`);
	return tableCell;
}

function createRow(dataObject) {
 	const tableRow = document.createElement('tr');
	tableRow.classList.add('table');
	tableRow.classList.add('table__tableRow');
    tableRow.setAttribute('id',`${dataObject.id}`)
	tableRow.setAttribute('onclick', `highLightRow(document.getElementById(${dataObject.id}))`);
	tableRow.appendChild(createCell(dataObject.id, 'table', 'table__tableCell', "table__tableCell-number"));
	tableRow.appendChild(createCell(dataObject.firstName, 'table', 'table__tableCell'));
	tableRow.appendChild(createCell(dataObject.lastName, 'table', 'table__tableCell'));
	tableRow.appendChild(createCell(dataObject.age, 'table', 'table__tableCell', "table__tableCell-number"));
	return tableRow;
}

function addStart(dataArray) {
    if (getData(dataArray)) {
        document.getElementById('table').prepend(createRow(dataArray[dataArray.length - 1]));
    } else {
        alert (`The row with id = ${document.getElementById('id').value} already exist!`)
    }
    
}

function addEnd(dataArray) {
    if (getData(dataArray)) {
        document.getElementById('table').append(createRow(dataArray[dataArray.length - 1]));
    } else {
        alert (`The row with id = ${document.getElementById('id').value} already exist!`)
    }
}

function addMiddle(dataArray) {
    
    if (getData(dataArray)) {
        if (document.getElementById('table').firstElementChild) {
            const rows = document.getElementsByTagName('tr');
            let averageRow = rows[Math.floor(rows.length / 2)];
            averageRow.after(createRow(dataArray[dataArray.length - 1]));
        } else addStart(dataArray);
    } else {
        alert (`The row with id = ${document.getElementById('id').value} already exist!`)
    }
    
}

function highLightRow(row) {
    const cells = row.getElementsByTagName('th');
    document.getElementById('id').value = cells[0].textContent;
    document.getElementById('firstName').value = cells[1].textContent;
    document.getElementById('lastName').value = cells[2].textContent;
    document.getElementById('age').value = cells[3].textContent;
}

function updateArrayItem(dataArray) {
    let isUpdated = 0;
    for (let index = 0; index < dataArray.length;  index++) {
        if (dataArray[index].id === document.getElementById('id').value) {
            dataArray[index].firstName =  document.getElementById('firstName').value;
            dataArray[index].lastName =  document.getElementById('lastName').value;
            dataArray[index].age =  document.getElementById('age').value;
        isUpdated = 1;
        updatedId = dataArray[index].id;
        }
    }
    if (isUpdated) {
        return dataArray;
    } else return void 0;
}

function updateRow(dataArray) {
    while (document.getElementById('table').firstChild) {
        document.getElementById('table').firstChild.remove()
    }

    dataArray.forEach(function(obj) {
       document.getElementById('table').append(createRow(obj));
    })
}

function deleteRow(dataArray) {

}

function saveData() {
	localStorage.setItem('', JSON.stringify(document.getElementById('table').innerHTML))
}

function readData() {
	document.getElementById('table').innerHTML = JSON.parse(localStorage.getItem('table'))
}

function clearData() {
	document.getElementById('table').innerHTML = null;
}


