describe('CRUD tests', () => {
	
	describe('getData function', () => {
		
		afterEach(() => {
			document.getElementById('testDiv').innerHTML = null;
		});
		const testData = [
			{
				valueOfId: '1',
				valueOfFirstName: 'Leonid',
				valueOfLastName: 'Makarovich',
				valueOfAge: '89',
			},
			{
				valueOfId: '2',
				valueOfFirstName: 'Leonid',
				valueOfLastName: 'Danylovich',
				valueOfAge: '81',
			},
			{
				valueOfId: '3',
				valueOfFirstName: 'Victor',
				valueOfLastName: 'Andrijovych',
				valueOfAge: '65',
			},
			{
				valueOfId: '4',
				valueOfFirstName: 'Victor',
				valueOfLastName: 'Fedorovych',
				valueOfAge: '69',
			},
			{
				valueOfId: '5',
				valueOfFirstName: 'Petro',
				valueOfLastName: 'Oleksiyovych',
				valueOfAge: '54',
			},
		
		];
		
		testData.forEach(data => {
			
			const {valueOfId, valueOfFirstName, valueOfLastName, valueOfAge} = data;
			
			it(`should return get correct data from input value`, () => {
				const testId = document.createElement('input');
				testId.setAttribute("id", "id");
				const testFirstName = document.createElement('input');
				testFirstName.setAttribute("id", "firstName");
				const testLastName = document.createElement('input');
				testLastName.setAttribute("id", "lastName");
				const testAge = document.createElement('input');
				testAge.setAttribute("id", "age");
				testId.value = valueOfId;
				testFirstName.value = valueOfFirstName;
				testLastName.value = valueOfLastName;
				testAge.value = valueOfAge;
				document.getElementById('testDiv').append(testId);
				document.getElementById('testDiv').append(testFirstName);
				document.getElementById('testDiv').append(testLastName);
				document.getElementById('testDiv').append(testAge);
				
				getData();
				
				assert.strictEqual(document.getElementById('id').value, valueOfId);
				assert.strictEqual(document.getElementById('firstName').value, valueOfFirstName)
				assert.strictEqual(document.getElementById('lastName').value, valueOfLastName)
				assert.strictEqual(document.getElementById('age').value, valueOfAge)
			});
		});
	});
	
	describe('createCell function', () => {
		
		afterEach(() => {
			document.getElementById('testDiv').innerHTML = null;
		});
		const testData = [
			{
				value: '1',
				class1: 'table',
				class2: 'table__tableCell',
				class3: 'table__tableCell-number',
			},
			{
				value: 'Leonid',
				class1: 'table',
				class2: 'table__tableCell',
			},
			{
				value: 'Danylovich',
				class1: 'table',
				class2: 'table__tableCell',
			},
			{
				value: '81',
				class1: 'table',
				class2: 'table__tableCell',
				class3: 'table__tableCell-number',
			},
		];
		
		testData.forEach(data => {
			
			const {value, class1, class2, class3} = data;
			
			it(`should createCell with ${value}, ${class1}, ${class2}, ${class3}`, () => {
				
				const expected = document.createElement('th');
				expected.innerHTML = `${value}`;
				expected.classList.add(`${class1}`);
				expected.classList.add(`${class2}`);
				expected.classList.add(`${class3}`);
				
				const actual = createCell(value, class1, class2, class3);
				
				assert.deepEqual(actual, expected);
			});
		});
	});
	
	describe('createRow function', () => {
		
		const testData = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		
		];
		
		testData.forEach(data => {
			
			const {id, firstName, lastName, age} = data;
			
			it(`should create row with ${id}, ${firstName}, ${lastName}, ${age}`, () => {
				const expected = document.createElement('tr');
				expected.classList.add('table');
				expected.classList.add('table__tableRow');
				expected.appendChild(createCell(id, 'table', 'table__tableCell', "table__tableCell-number"));
				expected.appendChild(createCell(firstName, 'table', 'table__tableCell',));
				expected.appendChild(createCell(lastName, 'table', 'table__tableCell'));
				expected.appendChild(createCell(age, 'table', 'table__tableCell', "table__tableCell-number"));
				
				const actual = createRow(id, firstName, lastName, age);
				
				assert.deepEqual(actual, expected);
			});
		});
	});
	
	describe('Save function', () => {
		
		const testData = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		
		];
		
			let {id, firstName, lastName, age} = testData[0];
			
			it(`should create row with ${id}, ${firstName}, ${lastName}, ${age}`, () => {
				const testId = document.createElement('input');
				testId.setAttribute("id", "id");
				testId.value = id;
				const testFirstName = document.createElement('input');
				testFirstName.setAttribute("id", "firstName");
				testFirstName.value = firstName;
				const testLastName = document.createElement('input');
				testLastName.setAttribute("id", "lastName");
				testLastName.value = lastName;
				const testAge = document.createElement('input');
				testAge.setAttribute("id", "age");
				testAge.value = age;
				const testTable = document.createElement('table');
				testTable.setAttribute('id', "table");
				document.getElementById('table').append(testTable);
				
				saveData();
				const actual = localStorage.getItem('table');
				
				assert.deepEqual(actual, JSON.stringify(testTable));
			});
		});
	

	let sandbox;
	
	before(() => {
		sandbox = sinon.createSandbox();
	});
	
	afterEach(() => {
		sandbox.restore();
	});
	describe('addStart function', () => {
		
		
		// afterEach(() => {
		// 	sandbox.restore();
		// 	document.getElementById('table').innerHTML = null;
		// });
		
		const testData = [
			{
				id: '1',
				firstName: 'Leonid',
				lastName: 'Makarovich',
				age: '89',
			},
			{
				id: '2',
				firstName: 'Leonid',
				lastName: 'Danylovich',
				age: '81',
			},
			{
				id: '3',
				firstName: 'Victor',
				lastName: 'Andrijovych',
				age: '65',
			},
			{
				id: '4',
				firstName: 'Victor',
				lastName: 'Fedorovych',
				age: '69',
			},
			{
				id: '5',
				firstName: 'Petro',
				lastName: 'Oleksiyovych',
				age: '54',
			},
		
		];
		
		testData.forEach(data => {
			
			const {id, firstName, lastName, age} = data;
			
			it('function getData called once', () => {
				const testId = document.createElement('input');
				testId.setAttribute("id", "id");
				testId.value = id;
				const testFirstName = document.createElement('input');
				testFirstName.setAttribute("id", "firstName");
				testFirstName.value = firstName;
				const testLastName = document.createElement('input');
				testLastName.setAttribute("id", "lastName");
				testLastName.value = lastName;
				const testAge = document.createElement('input');
				testAge.setAttribute("id", "age");
				testAge.value = age;
				const testTable = document.createElement('table');
				testTable.setAttribute('id', "table");
				document.getElementById('table').append(testTable);
				
				// const stub = sandbox.stub(window, 'getData');
				
				addStart();
				assert.deepEqual()
				// sandbox.assert.calledOnce(stub);
			});
			
			it('function alert called once', () => {
				const testId = document.createElement('input');
				testId.setAttribute("id", "id");
				const testFirstName = document.createElement('input');
				testFirstName.setAttribute("id", "firstName");
				const testLastName = document.createElement('input');
				testLastName.setAttribute("id", "lastName");
				const testAge = document.createElement('input');
				testAge.setAttribute("id", "age");
				const testTable = document.createElement('table');
				testTable.setAttribute('id', "table");
				document.getElementById('table').append(testTable);
				testId.value = '';
				testFirstName.value = 'Leonid';
				testLastName.value = 'Makarovych';
				testAge.value = '89';
				const stub = sandbox.stub(window, 'alert');
				
				addStart();
				
				sandbox.assert.calledOnce(stub);
				// sandbox.assert.calledWith(stub, 'Input all data, please!');
			});
		});
	});

    // describe('test setBegin', () => {
    //
    //     it('function setBegin calledOnce function beginSquare', () => {
    //         const stub = sandbox.stub(window, 'beginSquare');
    //         buttonType = 'square';
    //
    //         setBegin(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //     });
    //
    //     it('function setBegin calledOnce function beginCircle', () => {
    //         const stub = sandbox.stub(window, 'beginCircle');
    //         buttonType = 'oval';
    //
    //         setBegin(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //     });
    //
    //     it('function setBegin calledOnce function beginCurve', () => {
    //         const stub = sandbox.stub(window, 'beginCurve');
    //         buttonType = 'curve';
    //
    //         setBegin(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //     });
    //
    //     it('function setBegin calledOnce function beginLine', () => {
    //         const stub = sandbox.stub(window, 'beginLine');
    //         buttonType = 'line';
    //
    //         setBegin(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //     });
    // });
    //
    // describe('test setEnd', () => {
    //
    //     it('function setEnd calledOnce function endSquare', () => {
    //         const stub = sandbox.stub(window, 'endSquare');
    //         buttonType = 'square';
    //
    //         setEnd(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //         sandbox.assert.calledWith(stub, 'square');
    //     });
    //
    //     it('function setEnd calledOnce function endCircle', () => {
    //         const stub = sandbox.stub(window, 'endCircle');
    //         buttonType = 'oval';
    //
    //         setEnd(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //         sandbox.assert.calledWith(stub, 'oval');
    //     });
    //
    //     it('function setEnd calledOnce function endCurve', () => {
    //         const stub = sandbox.stub(window, 'endCurve');
    //         buttonType = 'curve';
    //
    //         setEnd(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //         sandbox.assert.calledWith(stub, 'curve');
    //     });
    //
    //     it('function setEnd calledOnce function endLine', () => {
    //         const stub = sandbox.stub(window, 'endLine');
    //         buttonType = 'line';
    //
    //         setEnd(buttonType);
    //
    //         sandbox.assert.calledOnce(stub);
    //         sandbox.assert.calledWith(stub, 'line');
    //     });
    // });

});