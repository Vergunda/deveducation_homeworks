 describe('sumOrMultiply', () => {
    const testData = [
      {
        a: 2,
        b: -3,
        expected: -6,
      },
      {
        a: 1,
        b: 2.5,
        expected: 3.5,
      },
      {
        a: undefined,
        b: 2,
        expected: false,
      },
      {
        a: 0,
        b: 2.5,
        expected: 0,
      }
    ];

    testData.forEach(data => {

      const {a,b,expected} = data;

    	it(`should return ${expected} when a = ${a} and b = ${b}`, () => {

      	const actual = sumOrMultiply(a, b);

      	assert.strictEqual(actual, expected);

	    });
  	});
  });