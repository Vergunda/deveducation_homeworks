function sumOrMultiply(a, b) {
	if (typeof(a) === "number" && typeof(b) === "number") {
		if (a % 2 === 0) {
			return a * b;
		} else {
			return a + b;
		}
	}
	else return false
}

function quaterOfCoordinates(x, y)	{
	if (typeof(x) === 'number' && typeof(y) === 'number') {
		if (x > 0 && y > 0) {
		return 1;
			} else if (x < 0 && y > 0 ){
		return 2;
			} else if (x < 0 && y < 0 ){
		return 3;
			} else if (x > 0 && y < 0 ){
		return 4;
			} if (x === 0 || y === 0){
		return "You are in coordinate axis";
		}
  } else return false;
};

function positiveNumberSum(a,b,c) {
		if (typeof(a) === 'number' && typeof(b) === 'number' && typeof(c) === 'number'){
			let sum = 0;
		  if (a > 0) {sum = sum + a};
		  if (b > 0) {sum = sum + b};
		  if (c > 0) {sum = sum + c};
		  return sum;
		} else return false;
}

function maxSumOrMultiply(a,b,c) {
	if (typeof(a) === 'number' && typeof(b) === 'number' && typeof(c) === 'number'){
		if ( (a + b + c ) > a * b * c) {
			return a + b + c + 3;
		} else return a * b * c + 3;
	} else return false;
}

function rating(score) {
		if (typeof(score) === 'number') {
				if ( score >= 0 && score < 20) {return "F"
		    } else if ( score >= 20 && score < 40 ) { return "E"
		    } else if ( score >= 40 && score < 60 ) { return "D"
		    } else if ( score >= 60 && score < 75 ) { return "C"
		    } else if ( score >= 75 && score < 90 ) { return "B"
		    } else if ( score >= 90 && score <= 100 ) { return "A"
			} else return "You input wrong score" ;
		} else return false;
}

function paritySumAndAmount()	{
	let sumParity = 0;
	let amountParity = 0;
	for (i = 1; i < 100; i++) 	{
			if (i % 2 == 0)	{
				sumParity = sumParity + i;
				amountParity = ++amountParity;
			}
		}
	return [sumParity, amountParity];
}

function isSimpleNumber(number) {
	if (typeof(number) === 'number' && number > 0) {
			if (number === 2) {
				return true;
			} else {
			for (i = 2; i < Math.sqrt(number); i++) {
				if (number % i === 0) {
					 return false
					 break;
				 }
				}
			return true;
			}
	} else return 'You input not a number'
}

function integerSquare(number) {
	if (typeof(number) === 'number' && number > 0) {
	let i = 0;
		while (i*i < number) { i++;}
		if (((i**2 - (i - 1)**2)/2)**2 > number) {
			return i - 1
		} else return i;
} else return false;

}

function factorial(n) {
	if (Number.isInteger(n) && n > 0) {
			let factorial = 1;
				for (i = 1; i <= n; i++) {
					factorial = factorial * i;
				}
			return factorial;
	} else return false;
}

function sumOfDigits(number) {
	if (Number.isInteger(number)) {
	 let sum = 0;
	 let mod = 0;
	 if (number < 0) {
		 number *= (-1);
	 }
			while (number > 0) {
  			mod = number % 10;
  			sum = sum + mod;
  			number = (number - +mod) / 10;
			}
		return sum;
	} else return false;
}

function mirrorDigits(number) {
	if (Number.isInteger(number)) {
		 if (number < 0) {
			 number *= (-1);
		 }
			let inverse = 0;
			let digit = 1;
			for (i = number.toString().length; i > 0; i--) {
							for (j = i - 1; j > 0; j--) {
								digit = digit * 10;
							}
					inverse = inverse + (number % 10) * digit;
					number = (number - (number % 10)) / 10;
					digit = 1;
			}
		  return inverse;
	} else return false;
}

function minArrayElement(arr) {
	if (Array.isArray(arr)) {
			let min = arr[0];
			for (i = 1; i < arr.length; i++) {
				if (arr[i] < min) {
					min = arr[i];
				}
			}
			return min;
	} else return false;
}

function maxArrayElement(arr) {
	if (Array.isArray(arr)) {
		let max = arr[0];
		for (i = 1; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		return max;
	} else return false;
}

function minIdArrayElement(arr) {
	if (Array.isArray(arr)) {
		let min = 0;
		for (i = 1; i < arr.length; i++) {
			if (arr[i] < arr[min]) {
				min = i;
			}
		}
		return min;
	} else return false;
}

function maxIdArrayElement(arr) {
	if (Array.isArray(arr)) {
		let max = 0;
		for (i = 1; i < arr.length; i++) {
			if (arr[i] > arr[max]) {
				max = i;
				}
		  }
		return max;
	} else return false;
}

function sumNoParityId(arr) {
	if (Array.isArray(arr)) {
		let sum = 0;
		for (i = 0; i < arr.length; i++) {
			if (i % 2 != 0) {
				sum = sum + arr[i];
			}
		}
		return sum;
	} else return false;
}

function reverseArray(arr) {
	if (Array.isArray(arr)) {
		let reverse = [];
		for (i = 0; i <= (arr.length - 1); i++) {
			reverse[i] = arr[(arr.length - 1)-i]
		}
		return reverse;
	} else return false;

}

function numberOfNoParity(arr) {
	if (Array.isArray(arr)) {
		let amount= 0;
		for (i = 0; i < arr.length; i++) {
			if (arr[i] % 2 != 0) {
			 amount = amount + 1;
			}
		}
		return amount;
	} else return false;
}

function changeArrayElements(arr) {
	if (Array.isArray(arr)) {
		let arrFirst = [];
		let arrSecond = [];
		for (i = 0; i < arr.length; i++) {
			if (i < arr.length/2) {
				arrFirst[i] = arr[i];
			} else {
				arrSecond[i - Math.ceil(arr.length/2)] = arr[i];
			}
		}
		return arrSecond.concat(arrFirst);
	} else return false;
}

function sortArrayBubble(arr) {
	if (Array.isArray(arr)) {
		let temp;
				for (j = arr.length; j > 0; j--) {
					for (i = 1; i < j; i++) {
						 if (arr[i - 1] > arr[i])	{
								temp = arr[i];
								arr[i] = arr[i - 1];
								arr[i - 1] = temp;
							}
					}
				}
				return arr;
	} else return false;
}


function sortArrayInsert(arr) {
	if (Array.isArray(arr)) {
		let temp;
		for (j = 1; j < arr.length; j++) {
			temp = arr[j];
			i = j - 1;
			while (i >= 0 && arr[i] > temp)	{
	        arr[i + 1] = arr[i];
					i--;
	  		}
	  		arr[i + 1] = temp;
	  	}
	  return arr;
	} else return false;
}

function sortArraySelect(arr) {
	if (Array.isArray(arr)) {
		let temp;
		let tempId;
	    for (i = 0; i < arr.length; i++) {
	    		tempId = i;
	    		for (k = i; k < arr.length; k++)
	    			{if (arr[tempId] > arr[k]) {
							tempId = k;
						}
	    		}
	    		temp = arr[i];
	    		arr[i] = arr[tempId];
	    		arr[tempId] = temp;
	    	}
			return arr;
	} else return false;
}

function sortArrayQuick(arr) {
	if (Array.isArray(arr)) {
		if (arr.length === 0) return [];
		let leftArray = [];
		let rightArray = [];
		let pivot = arr[0];
		for (var i = 1; i < arr.length; i++) {
			 if (arr[ i ] < pivot) {
				 leftArray[leftArray.length] = arr[ i ];
			 } else {
				 rightArray[rightArray.length] = arr[ i ];
		 	 }
		 }
		return sortArrayQuick(leftArray).concat(pivot,sortArrayQuick(rightArray) );
	} else return false;
}

function sortArrayShell(arr) {
	if (Array.isArray(arr)) {
		let pivotId = Math.floor(arr.length/2);
		let temp;
		 while (pivotId > 0) {
			for (j = 0; j < arr.length; j++) {
				let k = j;
				temp = arr[j];
					while (k >= pivotId && arr[k-i] > temp)
					 { arr[k] = arr[k-i]; k -= i; }
					arr[k] = temp;
				}
			pivotId = (pivotId==2) ? 1 : Math.floor(pivotId*5/11);
		 }
		return arr;
	} else return false;
  }

/*function sortArrayMerge(arr) {


	if (Array.isArray(arr)) {

	} else return false;
}

function Merge(a,low,mid,high)    //Вспомогательная функция.
{
    var b = new Array(high+1-low), h, i, j = mid+1, k, h = low, i = 0;
    while (h <= mid && j <= high )
     { if (a[h] <= a[j]){ b[ i ]=a[h]; h++; }
       else             { b[ i ]=a[j]; j++; }
       i++;
     }
    if (h > mid){ for (k = j; k <= high; k++){ b[ i ]=a[k]; i++; } }
    else        { for (k = h; k <= mid; k++){  b[ i ]=a[k]; i++; } }
    for (k=0; k<=high-low; k++) a[k+low]=b[k];
    return a;
}

function MergeSort(A)      //Функция сортировки слиянияем.
{
    function merge_sort(a,low,high)
     { if (low < high)
        { var mid = Math.floor((low+high)/2);
          merge_sort(a, low, mid);
          merge_sort(a, mid+1, high);
          Merge(a, low, mid, high);
        }
     }
    var n = A.length;
    merge_sort(A, 0, n-1);
    return A;
}
/*function sortArrayMerge(arr) {
	if (arr.length == 1) {
		return arr;
  } else {
		let leftArray = [];
		let rightArray = [];
		for (i = 0; i < arr.length; i++) {
			if (i < arr.length/2) {
				leftArray[i] = arr[i];
			} else {
				rightArray[i] = arr[i];
			}
		}
	leftArray = sortArrayMerge(leftArray);
	rightArray = sortArrayMerge(rightArray);
  return merge(leftArray,rightArray);
	}
}*/
/*
function merge(leftArray,rightArray) {
	let sortedArray = [];
	while (leftArray.length >= 0|| rightArray.length >=0) {
		if (leftArray[0] > rightArray[0]) {
			sortedArray.push(rightArray[0]);
			rightArray.shift();
		} else {
			sortedArray.push(leftArray[0]);
			leftArray.shift();
		}
	}
	while (leftArray.length >= 0) {
		sortedArray.push(leftArray[0]);
		leftArray.shift();
	}
	while (rightArray.length >= 0) {
		sortedArray.push(rightArray[0]);
		rightArray.shift();
	}
	return sortedArray;
}

function HeapSort(A) {
    if (A.length == 0) return [];
    var n = A.length, i = Math.floor(n/2), j, k, t;
    while (true)
    { if (i > 0) t = A[--i];
      else { n--;
             if (n == 0) return A;
             t = A[n];  A[n] = A[0];
           }
      j = i;  k = j*2+1;
      while (k < n)
       { if (k+1 < n && A[k+1] > A[k]) k++;
         if (A[k] > t)
          { A[j] = A[k];  j = k;  k = j*2+1; }
         else break;
       }
      A[j] = t;
    }
}

	function Merge(a,low,mid,high)  {
	    var b = new Array(high+1-low), h, i, j = mid+1, k, h = low, i = 0;
	    while (h <= mid && j <= high )
	     { if (a[h] <= a[j]){ b[ i ]=a[h]; h++; }
	       else             { b[ i ]=a[j]; j++; }
	       i++;
	     }
	    if (h > mid){ for (k = j; k <= high; k++){ b[ i ]=a[k]; i++; } }
	    else        { for (k = h; k <= mid; k++){  b[ i ]=a[k]; i++; } }
	    for (k=0; k<=high-low; k++) a[k+low]=b[k];
	    return a;
	}

	function MergeSort(A) {
	    function merge_sort(a,low,high)
	     { if (low < high)
	        { var mid = Math.floor((low+high)/2);
	          merge_sort(a, low, mid);
	          merge_sort(a, mid+1, high);
	          Merge(a, low, mid, high);
	        }
	     }
	    var n = A.length;
	    merge_sort(A, 0, n-1);
	    return A;
	}
*/


const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const dayOfWeek = (n) => ( (n ^ 0) === n && n > 0 && n < 8) ? week[n - 1] : "You input wrong number";

const lengthBetweenDots = (x1,y1,x2,y2) => (typeof(x1) === 'number' && typeof(y1) === 'number' && typeof(x2) === 'number' && typeof(y2) === 'number') ? Math.sqrt((x1 - x2)**2 + (y1 - y2)**2) : false;



//Classwork

function getObject(arr) {
  const obj = {};
  for (i = 0; i < arr.length; i++) {
    obj[arr[i]] = 0;
  }
 return obj;
}

const getResult = () => Math.random() > 0.5 ? 'URA': 'NE URA';

const getType = (param) => typeof(param);

const getWholeString = (a,b,c) => a + b + c;
