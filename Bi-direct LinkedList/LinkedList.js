function Node(value) {
    this.value = value;
    this.next = null;
    this.pervious = null;
    }

function LinkedList() {
    this.head = null;
    this.size = 0;
}

// getLastNode - return last node

LinkedList.prototype.getLastNode = function () {

    if (!this.head) {
        return null;
    } else {
        let lastNode = this.head;
        
        while (lastNode.next) {
            lastNode = lastNode.next;
        }

        return lastNode;
    }
};

// init(array) - initialize by provided array

LinkedList.prototype.init = function (array) {
    
    this.head = null;
    this.size = 0;
    
    let perviousNode;
    
    for (let index = 0; index < array.length; index++) {
        
        const node = new Node(array[index]);
        
        if (index === 0) {
            this.head = node;
            perviousNode = node;
        } else {
            perviousNode.next = node;
            node.pervious = perviousNode;
            perviousNode = node;
        }
       
        this.size++;
    }
};

// toString() returns string in format '{ 1, 2, 3, 4 }'

LinkedList.prototype.toString = function() {
    
    let result = '';
    
    if(!this.head) {
        return result;
    } else {
        let node = this.head;
        
        while (node.next) {
            result += `${node.value}, `;
            node = node.next;
        }
    
        result += `${node.value}, `
    
        return result;
    }
};

// push(value) add element to the end of list'

LinkedList.prototype.push = function(value){
    const lastNode = this.getLastNode();
    const node = new Node(value);
    if (!lastNode) {
        this.head = node;
    } else {
        lastNode.next = node;
        node.pervious = lastNode;
    }
    this.size++;
};

// pop(); // delete element from end and returns it value

LinkedList.prototype.pop = function () {
    
    if (!this.head) {
        return null;
    } else {
        const lastNode = this.getLastNode();
        const popValue = lastNode.value;
        
        if (!lastNode.pervious) {
            this.head = null;
        } else {
            lastNode.pervious.next = null;
        }
        
        this.size--;
        return popValue;
        }
};

// shift(); // delete element from start and returns it value

LinkedList.prototype.shift = function() {
    
    if (!this.head) {
        return null;
    } else {
        const deletedValue = this.head.value;
        
        if (!this.head.next) {
            this.head = null;
            this.size = 0;
            return deletedValue;
        } else {
            this.head.next.pervious = null;
            this.head = this.head.next;
            this.size--;
            return deletedValue;
        }
    }
};

// unshift(value); // add element to the start of list

LinkedList.prototype.unshift = function(value) {
    
    let node = new Node(value);
    
    if (this.head) {
        this.head.pervious = node;
        node.next = this.head;
    }
    
    this.head = node;
    this.size++;
};

// set(index, value); // set value by index

LinkedList.prototype.set = function (index, value) {
    
    if (index > this.size) {
        return undefined;
    } else if (index === 0) {
        this.unshift(value);
    } else if (index === 1) {
        const newNode = new Node(value);
        newNode.next = this.head.next;
        newNode.pervious = this.head;
        newNode.next.pervious = newNode;
        this.head.next = newNode;
        this.size++;
    } else if (index === this.size) {
        this.push(value);
    } else {
            let tempNode = this.head.next;
            const newNode = new Node(value);
            let setIndex = 2;
            
            while (tempNode.next) {
                if (setIndex === index) {
                    newNode.next = tempNode;
                    newNode.pervious = tempNode.pervious;
                    newNode.pervious.next = newNode;
                    tempNode.pervious = newNode;
                    break;
                }
                setIndex++;
                tempNode = tempNode.next;
            }
            this.size++;
    }
};

// get(index); // return value by index

LinkedList.prototype.get = function(index) {
    
    if (!this.head) {
        return undefined;
    } else if (index >= this.size) {
        return undefined;
    } else {
        let node = this.head;
        let getIndex = 0;
        
        while (node.next) {
            node = node.next;
            getIndex++;
        
            if (getIndex === index) {
                break;
            }
        }
        return node.value;
    }
};

let linkedList = new LinkedList();
