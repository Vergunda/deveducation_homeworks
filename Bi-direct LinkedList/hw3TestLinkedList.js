describe('LinkedList Tests', () => {
    
    describe('getLastNode() - returns last node of list', () => {
        const testData = [
            {
                array: [],
                expected: null,
            },
            {
                array: [1],
                expected: {value: 1, next: null, pervious: null},
            },
            {
                array: [-5, 0],
                expected: {value: 0, next: null, pervious: -5},
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: {value: 13, next: null, pervious: 0},
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: {value: 3, next: null, pervious: 13},
            },
        ];
        it(`should return null when array []`, () => {
            const {array, expected} = testData[0];

            linkedList.init(array);
            const actual = linkedList.getLastNode();
          
            assert.deepEqual(actual, expected);
        });
        it(`should return {value: 1, next: null, pervious: null} when array [1]`, () => {
            const {array, expected} = testData[1];

            linkedList.init(array);
            const actual = linkedList.getLastNode();

            assert.deepEqual(actual, expected);
        });
        it(`should return {value: 0, next: null, pervious: -5} when array [-5,0]`, () => {
            const {array, expected} = testData[2];

            linkedList.init(array);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};

            assert.deepEqual(actual, expected);
        });
        it(`should return {value: 13, next: null, pervious: 0} when array [-5, 20, 59, 0, 13]`, () => {
            const {array, expected} = testData[3];

            linkedList.init(array);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};

            assert.deepEqual(actual, expected);
        });
        it(`should return {value: 3, next: null, pervious: 13} when array [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected} = testData[4];

            linkedList = new LinkedList();
            linkedList.init(array);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};

            assert.deepEqual(actual, expected);
        });

    });

    describe('toString() - returns string in format \'{ 1, 2, 3, 4 }', () => {
        const testData = [
            {
                array: [],
                expected: '',
            },
            {
                array: [1],
                expected: '1, ',
            },
            {
                array: [-5, 0],
                expected: '-5, 0, ',
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: '-5, 20, 59, 0, 13, '
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: '-5, 20, 59, 0, 13, 3, '
            },
        ];
        it(`should return [] when array = ''`, () => {
            const {array, expected} = testData[0];
            
            linkedList.init(array);
            const actual = linkedList.toString();
            
            assert.strictEqual(actual, expected);
        });
        it(`should return [1] when array = '1,'`, () => {
            const {array, expected} = testData[1];
            
            linkedList.init(array);
            const actual = linkedList.toString();
            
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 0] when array = '-5, 0,'`, () => {
            const {array, expected} = testData[2];

            linkedList.init(array);
            const actual = linkedList.toString();
            
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13] when array = '-5,20,59,0,13,'`, () => {
            const {array, expected} = testData[3];
            
            linkedList.init(array);
            const actual = linkedList.toString();
            
            assert.strictEqual(actual, expected);
        });
        it(`should return [-5, 20, 59, 0, 13, 3] when array = '-5,20,59,0,13,3,'`, () => {
            const {array, expected} = testData[4];
            
            linkedList.init(array);
            const actual = linkedList.toString();
            
            assert.strictEqual(actual, expected);
        });
    });
    
    describe('push(value) - add element to the end of list', () => {
        const testData = [
            {
                array: [],
                value: 1,
                expected: {value: 1, next: null, pervious: null},
                size: 1,
            },
            {
                array: [1],
                value: 2,
                expected: {value: 2, next: null, pervious: 1},
                size: 2,
            },
            {
                array: [-5, 0],
                value: 3,
                expected: {value: 3, next: null, pervious: 0},
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                expected: {value: 4, next: null, pervious: 13},
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                expected: {value: 5, next: null, pervious: 3},
                size: 7,
            },
        ];
        
        it(`should return {value: 1, next: null, pervios: null} and size 1, when array = [] and value = 1`, () => {
            const {array, value, expected, size} = testData[0];
            
            linkedList.init(array);
            linkedList.push(value);
            const actual = linkedList.getLastNode();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return {value: 2, next: null, pervios: 1} and size 2, when array = [1] and value = 2`, () => {
            const {array, value, expected, size} = testData[1];
            
            linkedList.init(array);
            linkedList.push(value);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return {value: 3, next: null, pervios: 0} and size 3, when array = [-5, 0] and value = 3`, () => {
            const {array, value, expected, size} = testData[2];
            
            linkedList.init(array);
            linkedList.push(value);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return {value: 4, next: null, pervios: 13}, when array = [-5, 20, 59, 0, 13] and value = 4`, () => {
            const {array, value, expected, size} = testData[3];
            
            linkedList.init(array);
            linkedList.push(value);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return {value: 5, next: null, pervios: 3}, when array = [-5, 20, 59, 0, 13, 3] and value = 5`, () => {
            const {array, value, expected, size} = testData[4];
            
            linkedList.init(array);
            linkedList.push(value);
            const lastNode = linkedList.getLastNode();
            const actual = {value: lastNode.value, next: null, pervious: lastNode.pervious.value};
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
    });
    
    describe('pop() - delete element from end and returns it value', () => {
        const testData = [
            {
                array: [],
                expected: null,
                lastNode: null,
                size: 0,
            },
            {
                array: [1],
                expected: 1,
                lastNode: null,
                size: 0,
            },
            {
                array: [-5, 0],
                expected: 0,
                lastNode: {value: -5, next: null, pervious: null},
                size: 1,
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: 13,
                lastNode: {value: 0, next: null, pervious: 59},
                size: 4,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: 3,
                lastNode: {value: 13, next: null, pervious: 0},
                size: 5,
            },
        ];
        
        it(`should return value = null, lastNode = null and size 0, when array = []`, () => {
            const {array, expected, lastNode, size} = testData[0];
            
            linkedList.init(array);
            const actual = linkedList.pop();
            const lastNodeAfterPop = linkedList.getLastNode();
            
            assert.deepEqual(actual, expected);
            assert.deepEqual(lastNodeAfterPop, lastNode);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: 1, lastNode = null and size 0, when array = [1]`, () => {
            const {array, expected, lastNode, size} = testData[1];
            
            linkedList.init(array);
            const actual = linkedList.pop();
            const lastNodeAfterPop = linkedList.getLastNode();
            
            assert.deepEqual(actual, expected);
            assert.deepEqual(lastNodeAfterPop, lastNode);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: 0, lastNode = {value: 5, next: null, pervious: null} and size 1, when array = [-5, 0]`, () => {
            const {array, expected, lastNode, size} = testData[2];
            
            linkedList.init(array);
            const actual = linkedList.pop();
            const lastNodeAfterPop = linkedList.getLastNode();
    
            assert.deepEqual(actual, expected);
            assert.deepEqual(lastNodeAfterPop, lastNode);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: 13, lastNode = {value: 0, next: null, pervious: 59} and size 4 when array = [-5, 20, 59, 0, 13]`, () => {
            const {array, expected, lastNode, size} = testData[3];
            
            linkedList.init(array);
            const actual = linkedList.pop();
            const lastNodeAfterPop = {value: linkedList.getLastNode().value, next: null, pervious: linkedList.getLastNode().pervious.value}
            
            assert.deepEqual(actual, expected);
            assert.deepEqual(lastNodeAfterPop, lastNode);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: 3, lastNode = {value: 13, next: null, pervious: 0} and size 5, when array = [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected, lastNode, size} = testData[4];

            linkedList.init(array);
            const actual = linkedList.pop();
            const lastNodeAfterPop = {value: linkedList.getLastNode().value, next: null, pervious: linkedList.getLastNode().pervious.value}
    
    
            assert.deepEqual(actual, expected);
            assert.deepEqual(lastNodeAfterPop, lastNode);
            assert.strictEqual(size, linkedList.size);
        });
        
    });
    
    describe('shift() - delete element from start and returns it value', () => {
        const testData = [
            {
                array: [],
                expected: null,
                head: null,
                size: 0,
            },
            {
                array: [1],
                expected: 1,
                head: null,
                size: 0,
            },
            {
                array: [-5, 0],
                expected: -5,
                head: 0,
                size: 1,
            },
            {
                array: [-5, 20, 59, 0, 13],
                expected: -5,
                head: 20,
                size: 4,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                expected: -5,
                head: 20,
                size: 5,
            },
        ];
        
        it(`should return value = null, head = null and size 0, when array = []`, () => {
            const {array, expected, head, size} = testData[0];

            linkedList.init(array);
            const actual = linkedList.shift();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(head, linkedList.head);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: 1, head = null and size 0, when array = [1]`, () => {
            const {array, expected, head, size} = testData[1];

            linkedList.init(array);
            const actual = linkedList.shift();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(head, linkedList.head);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: -5, head = 0 and size 1, when array = [-5, 0]`, () => {
            const {array, expected, head, size} = testData[2];

            linkedList.init(array);
            const actual = linkedList.shift();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(head, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: -5, head = 20 and size 4 when array = [-5, 20, 59, 0, 13]`, () => {
            const {array, expected, head, size} = testData[3];

            linkedList.init(array);
            const actual = linkedList.shift();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(head, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return value: -5 head = 20 and size 5, when array = [-5, 20, 59, 0, 13, 3]`, () => {
            const {array, expected, head, size} = testData[4];

            linkedList.init(array);
            const actual = linkedList.shift();
            
            assert.deepEqual(actual, expected);
            assert.strictEqual(head, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
    });
    
    describe('unshift(value) - add element to the start of list', () => {
        const testData = [
            {
                array: [],
                value: 1,
                expected: 1,
                size: 1,
            },
            {
                array: [1],
                value: 2,
                expected: 2,
                size: 2,
            },
            {
                array: [-5, 0],
                value: 3,
                expected: 3,
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                expected: 4,
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                expected: 5,
                size: 7,
            },
        ];
        
        it(`should return head.value = 1 and size 1, when array = [] and value 1`, () => {
            const {array, value, expected, size} = testData[0];
            
            linkedList.init(array);
            linkedList.unshift(value);
            
            assert.strictEqual(expected, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return head.value: 2 and size 2, when array = [1] and value 2`, () => {
            const {array, value, expected, size} = testData[1];
            
            linkedList.init(array);
            linkedList.unshift(value);
            
            assert.strictEqual(expected, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return head.value: 3 and size 3, when array = [-5, 0] and value = 3`, () => {
            const {array, value, expected, size} = testData[2];
            
            linkedList.init(array);
            linkedList.unshift(value);
            
            assert.strictEqual(expected, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return head.value: 4 and size 6 when array = [-5, 20, 59, 0, 13] and value = 4`, () => {
            const {array, value, expected, size} = testData[3];
            
            linkedList.init(array);
            linkedList.unshift(value);
            
            assert.strictEqual(expected, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return head.value: 5 and size 7, when array = [-5, 20, 59, 0, 13, 3] and value = 5`, () => {
            const {array, value, expected, size} = testData[4];
            
            linkedList.init(array);
            linkedList.unshift(value);
            
            assert.strictEqual(expected, linkedList.head.value);
            assert.strictEqual(size, linkedList.size);
        });
    });
    
    describe('get(index) - set value by index', () => {
        const testData = [
            {
                array: [],
                index: 0,
                expected: undefined,
            },
            {
                array: [1],
                index: 1,
                expected: undefined,
            },
            {
                array: [-5, 0],
                index: 1,
                expected: 0,
            },
            {
                array: [-5, 20, 59, 0, 13],
                index: 2,
                expected: 59,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                index: 4,
                expected: 13,
            },
        ];
        
        it(`should return undefined, when array = [] and index = 1`, () => {
            const {array, index, expected} = testData[0];
            
            linkedList.init(array);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
        });
        
        it(`should return undefined, when array = [1] and index = 1`, () => {
            const {array, index, expected} = testData[1];
            
            linkedList.init(array);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 0, when array = [-5, 0] and index = 1`, () => {
            const {array, index, expected} = testData[2];
            
            linkedList.init(array);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 59, when array = [-5, 20, 59, 0, 13],and index = 2`, () => {
            const {array, index, expected} = testData[3];
            
            linkedList.init(array);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
        });
        
        it(`should return 13, when array = [-5, 20, 59, 0, 13, 3],and index = 4`, () => {
            const {array, index, expected} = testData[4];
            
            linkedList.init(array);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
        });
        
    });
    
    describe('set(value,index) - set value by index', () => {
        const testData = [
            {
                array: [],
                value: 1,
                index: 0,
                expected: 1,
                size: 1,
            },
            {
                array: [1],
                value: 2,
                index: 2,
                expected: undefined,
                size: 1,
            },
            {
                array: [-5, 0],
                value: 1,
                index: 1,
                expected: 1,
                size: 3,
            },
            {
                array: [-5, 20, 59, 0, 13],
                value: 4,
                index: 3,
                expected: 4,
                size: 6,
            },
            {
                array: [-5, 20, 59, 0, 13, 3],
                value: 5,
                index: 4,
                expected: 5,
                size: 7,
            },
        ];
        
        it(`should return 1 and size 1, when array = [] anf value = 1, index = 0`, () => {
            const {array, value, index, expected, size} = testData[0];
            
            linkedList.init(array);
            linkedList.set(index, value);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return undefined and size 2, when array = [1] anf value = 2, index = 2`, () => {
            const {array, value, index, expected, size} = testData[1];
            
            linkedList.init(array);
            linkedList.set(index, value);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return 1 and size 3, when array = [-5, 0] anf value = 1, index = 1`, () => {
            const {array, value, index, expected, size} = testData[2];
            
            linkedList.init(array);
            linkedList.set(index, value);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return 4 and size 6, when array = [-5, 20, 59, 0, 13],and value = 4, index = 3`, () => {
            const {array, value, index, expected, size} = testData[3];
            
            linkedList.init(array);
            linkedList.set(index, value);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
        it(`should return 5 and size 7, when array = [-5, 20, 59, 0, 13, 3],and value = 5, index = 4`, () => {
            const {array, value, index, expected, size} = testData[4];
            
            linkedList.init(array);
            linkedList.set(index, value);
            const actual = linkedList.get(index);
            
            assert.strictEqual(actual, expected);
            assert.strictEqual(size, linkedList.size);
        });
        
    });
    
});


