
// 1) 1) Родитель - animal(age=10). Наследник - rabbit(jumps: true). rabbit => age, jumps
function animal() {
    this.age = 10;
}

const rabbit = new animal();

rabbit.jums = true;

//2) Родитель - animal(sayWho => console.log('animal')). Наследник - rabbit(sayWho => console.log('rabbit')).
// sayWho от Родитель, Наследник

function animal() {
    this.name = 'animal';
}

animal.prototype.sayWho = function() {
    console.log(this.name);
}

const rabbit = new animal();
rabbit.sayWho(); //animal
rabbit.name = 'rabbit';
rabbit.sayWho(); // rabbit


//3) Родитель - animal(age=10, name='animal'). Наследник - rabbit.

function animal(name, age) {
    this.name = name;
    this.age = age;
    }


const rabbit = new animal('rabbit', '10');

// wright

function animal(0) {
    this.age = 10;
    this.name = 'animal';
}

function rabbit(age, name) {
    animal.call(this);
    this.age = age;
    this.name = name;
}

// 4. Сущность user(name, surname). get/set fullname

function user(name, surname) {
    this.name = null;
    this.surname = null;
}
    user.prototype.set = function(name,surname) {
        this.name = name;
        this.surname = surname;
    }

    user.prototype.getFullName = function() {console.log (this.name + ' ' + this.surname)
}



////////////

const user = {
    store: '',

    set fullName(value) {
        this.store = value;
    },

    get fullName() {
        return this.store;
    },
}

///////нельзя добавлять новые

class test {
    constructor() {
        this.value = 10;
        Object.preventExtention(this);
    }

}

const test = new Test()

///////protection from delete

    class test {
        constructor() {
            this.value = 10;
            Object.seal(this);
        }

    }

const test = new Test()

////заморозка

class test {
    constructor() {
        this.value = 10;
        Object.freeze(this);
    }

}

const test = new Test()

/////
function API() {
    const api1 = () => {
        console.log('api1')
    }
    const api3 = () => {
        console.log('api2')
    }
    const api2 = () => {
        console.log('api2')
        api3();
    }
    return Object.freeze({
        api1,
        api2,
    })
}
const via = new API();

/////

sum = function(a) {
    return a + sum(b);


}

function Counter() {
    let counter = 0;
    this.up = () => ++counter;
    this.down = () => --counter;
}

let count = new Counter();
count.up();
count.up();
count.down();


counter = {
    counter: 0,
    up: function() {return ++this.counter},
    down: function() {return --this.counter},
};



function sum(a) {
    return function(b) {
        return function(c) {
            return a + b + c;
        }
    }
}

a = sum(3)(5)(6);


pow = function(n, x) {
    if ( x === 1) {
        return n;
    } else {
        return n * pow(n,x - 1)
    }
};

someTo = function(n) {
    if (n === 1) {
        return 1;
    } else {
        return n + someTo(n - 1);
    }
};

fibonacci = function (n) {

    if (n === 0 || n === 1) {
        return 1
    } else {
        return  fib = fibonacci(n - 1) + fibonacci(n - 2);
    }
};