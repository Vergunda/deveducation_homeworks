describe('calcTest', () => {

	describe('Addition', () => {
		const testData = [
			{
				a: 1,
				b: 2,
				expected: 3,
			},
			{
				a: -1,
				b: 2,
				expected: 1,
			},
			{
				a: 0,
				b: 2.5,
				expected: 2.5,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}, b = ${b}`, () => {
				const actual = calc.methods['+'](a, b);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Substraction', () => {
		const testData = [
			{
				a: 1,
				b: 2,
				expected: -1,
			},
			{
				a: 2,
				b: -1,
				expected: 3,
			},
			{
				a: 0,
				b: 2.5,
				expected: -2.5,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}, b = ${b}`, () => {
				const actual = calc.methods['-'](a, b);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Multiplication', () => {
		const testData = [
			{
				a: 12,
				b: 2,
				expected: 24,
			},
			{
				a: -1,
				b: 2,
				expected: -2,
			},
			{
				a: 0,
				b: 2.5,
				expected: 0,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}, b = ${b}`, () => {
				const actual = calc.methods['*'](a, b);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Division', () => {
		const testData = [
			{
				a: 1,
				b: 2,
				expected: 0.5,
			},
			{
				a: -13,
				b: 2,
				expected: -6.5,
			},
			{
				a: 0,
				b: 2.5,
				expected: 0,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}, b = ${b}`, () => {
				const actual = calc.methods['/'](a, b);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Square Root', () => {
		const testData = [
			{
				a: 81,
				expected: 9,
			},
			{
				a: 6.25,
				expected: 2.5,
			},
			{
				a: 0,
				expected: 0,
			},
			{
				a: 1,
				expected: 1,
			},
			{
				a: -1,
				expected: false,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}`, () => {
				const actual = calc.methods['sqrt'](a);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Square Pow', () => {
		const testData = [
			{
				a: 9,
				expected: 81,
			},
			{
				a: 2.5,
				expected: 6.25,
			},
			{
				a: 0,
				expected: 0,
			},
			{
				a: 1,
				expected: 1,
			},
			{
				a: -1,
				expected: 1,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}`, () => {
				const actual = calc.methods['pow2'](a);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('Pow', () => {
		const testData = [
			{
				a: 9,
				b: 3,
				expected: 729,
			},
			{
				a: -2.5,
				b: 3,
				expected: -15.625,
			},
			{
				a: 123,
				b: 1,
				expected: 123,
			},
			{
				a: -1.123,
				b: 0,
				expected: 1,
			},
			{
				a: -2,
				b: 4,
				expected: 16,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}, b = ${b}`, () => {
				const actual = calc.methods['pow'](a, b);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('1 / x button', () => {
		const testData = [
			{
				a: 10,
				expected: 0.1,
			},
			{
				a: 2.5,
				expected: 0.4,
			},
			{
				a: 0,
				expected: Infinity,
			},
			{
				a: 1,
				expected: 1,
			},
			{
				a: -2,
				expected: -0.5,
			},
		];

		testData.forEach(data => {

			const {a, b, expected} = data;

			it(`should return ${expected} when a = ${a}`, () => {
				const actual = calc.methods['1/x'](a);
				assert.strictEqual(expected, actual);
			});
		});
	});

	describe('+/- button', () => {
		const testData = [
			{
				string: '10',
				expected: '-10',
			},
			{
				string: '-0.5',
				expected: '0.5',
			},
			{
				string: '10 + 5',
				expected: '10 + -5',
			},
			{
				string: '0',
				expected: '0',
			},
		];

		testData.forEach(data => {

			const {string, expected} = data;

			it(`should return ${expected} when string = ${string}`, () => {
				const actual = insertMinus(string);
				assert.equal(expected, actual);
			});
		});
	});

	describe('percentage', () => {
		const testData = [
			{
				string: '10 + 2',
				expected: 10.2,
			},
			{
				string: '5 - 5',
				expected: 4.75,
			},
			{
				string: '1 / 4',
				expected: 25,
			},
			{
				string: '10 * 4',
				expected: 4,
			},
			{
				string: '0 * 4',
				expected: 0,
			},
			{
				string: '4 / 0',
				expected: Infinity,
			},
			{
				string: 'one + 2',
				expected: false,
			},
			{
				string: '3 + NaN',
				expected: false,
			},

		];

		testData.forEach(data => {

			const {string, expected} = data;

			it(`should return ${expected} when input data = ${string}`, () => {
				const actual = calc.percentage(string);
				assert.strictEqual(expected, actual);
			});
		});
	})

	describe('singleNumberCalculate', () => {
		const testData = [
			{
				string: '9',
				operator: 'pow2',
				expected: 81,
			},
			{
				string: '-81',
				operator: 'sqrt',
				expected: false,
			},
			{
				string: '0.25',
				operator: '1/x',
				expected: 4,
			},
			{
				string: '-2.5',
				operator: 'pow2',
				expected: 6.25,
			},
			{
				string: 'df81',
				operator: 'pow',
				expected: false,
			},
			{
				string: 'NaN',
				operator: 'pow',
				expected: false,
			},
		]

		testData.forEach(data => {

			const {string, operator, expected} = data;

			it(`should return ${expected} when a = ${string} and operator = ${operator}`, () => {
				const actual = calc.singleNumberCalculate(string, operator);
				assert.strictEqual(expected, actual);
			});
		});
	})

	describe('Calculator', () => {
		const testData = [
			{
				string: '1 + 2',
				expected: 3,
			},
			{
				string: '-1 - 2.5',
				expected: -3.5,
			},
			{
				string: '1 / 4',
				expected: 0.25,
			},
			{
				string: '0 * 4',
				expected: 0,
			},
			{
				string: '4 / 0',
				expected: Infinity,
			},
			{
				string: 'one + 2',
				expected: false,
			},
			{
				string: '3 + NaN',
				expected: false,
			},

		];

		testData.forEach(data => {

			const {string, expected} = data;

			it(`should return ${expected} when input data = ${string}`, () => {
				const actual = calc.calculate(string);
				assert.strictEqual(expected, actual);
			});
		});
	})

	describe('backspace', () => {
		const testData = [
			{
				string: '10 + 2',
				expected: '10 + ',
			},
			{
				string: '5 - 52',
				expected: '5 - 5',
			},
			{
				string: '0.2564',
				expected: '0.256',
			},
			{
				string: '-5',
				expected: '-',
			},
			{
				string: '4 - ',
				expected: '4',
			},
			{
				string: '4 + ',
				expected: '4',
			},
		];

		testData.forEach(data => {

			const {string, expected} = data;

			it(`should return ${expected} when input data = ${string}`, () => {
				const actual = backspace(string);
				assert.strictEqual(expected, actual);
			});
		});
	})

	describe('insert', () => {
		const testData = [
			{
				string: '5 - 52',
				number: '3',
				expected: '5 - 523',
			},
			{
				string: '0',
				number: '.',
				expected: '0.',
			},
			{
				string: '5',
				number: '0',
				expected: '50',
			},
		];

		testData.forEach(data => {

			const {string, number, expected} = data;

			it(`should return ${expected} when input data = ${string}, number = ${number}`, () => {
				const actual = insert(string, number);
				assert.strictEqual(expected, actual);
			});
		});
	})

	describe('operator', () => {
		const testData = [
			{
				string: '5 - 52',
				number: ' - ',
				expected: '-47 - ',
			},
			{
				string: '2.5 - ',
				number: ' + ',
				expected: '2.5 + ',
			},
			{
				string: '5 - 52',
				number: ' pow ',
				expected: '-47 pow ',
			},
		];

		testData.forEach(data => {

			const {string, number, expected} = data;

			it(`should return ${expected} when input data = ${string}, number = ${number}`, () => {
				const actual = operation(string, number);
				assert.equal(expected, actual);
			});
		});
	})
});
