const insert = (string, number) => string + number;

function operation(string, operator) {
	if (string.split(' ').length === 2) {
		return string.split(' ')[0] + operator;
	} else if (string.split(' ').length === 3) {
		return calc.calculate(string) + operator;
	} else return string + operator;
}

const backspace = (string) => string[string.length - 1] === ' ' ? string.slice(0, string.length - 3) : string.slice(0, string.length - 1);

function insertMinus(string) {
  const split = string.split(' ');
  if (split[2]) {
    return split[0] + ' ' + split[1] + ' ' + -split[2];
  } else if (split[1]) {
    return -split[0] + ' ' + split[1] + ' ';
  } else  return -split[0];
}

const clean = () => '';

function Calculator() {
    
    this.methods = {
        "-": (a, b) => a - b,
        "+": (a, b) => a + b,
        "*": (a, b) => a * b,
        "/": (a, b) => a / b,
        "sqrt": (a) => a < 0 ? false : Math.sqrt(a),
        "pow2": (a) => Math.pow(a, 2),
        "1/x": (a) => 1 / a,
        "pow": (a, b) => Math.pow(a, b),
        "sin": (a) => Math.sin(a),
        "cos": (a) => Math.cos(a),
        "tan": (a) => Math.tan(a),
        "ln": (a) => Math.log1p(a),
        "log": (a) => Math.log10(a),
        "pi": () => Math.PI,
        "e": () => Math.E,
        "Rad": (a) => a * Math.PI / 180,
        "Deg": (a) => a * 180 / Math.PI,
        "Inv": (a) => a >= 0 ? Math.round((a - Math.floor(a)) * 1000000) / 1000000 : Math.round((1 - a - Math.floor(a) * 1000000) / 1000000),
        "Exp": (a) => Math.pow(Math.E, a),
        "Ans": () => ans,
    };
    
    this.calculate = function (string) {
        
        const a = +string.split(' ')[0];
        const operator = string.split(' ')[1];
        const b = +string.split(' ')[2];
        
        if (!this.methods[operator] || isNaN(a) || isNaN(b) || (typeof (a) != 'number') || (typeof (b) != 'number')) {
            return false;
        } else return this.methods[operator](a, b);
    };
    
    this.equal = function(string) {
        ans = this.calculate(string);
        return ans;
    }
    
    this.singleNumberCalculate = function (string, operator) {
        if (!this.methods[operator] || isNaN(+string) || (typeof (+string) != 'number')) {
            return false;
        } else return this.methods[operator](+string);
    }
    
    this.percentage = function (string) {
        
        const a = +string.split(' ')[0];
        const operator = string.split(' ')[1];
        const b = +string.split(' ')[2];
        
        if (!this.methods[operator] || isNaN(a) || isNaN(b) || (typeof (a) != 'number') || (typeof (b) != 'number')) {
            return false;
        } else return this.methods[operator](a, (a * b / 100))
    };
    
    this.factorial = function(number) {
        if (number === 1) {
            return 1;
        } else {
            return number * this.factorial(number - 1);
        }
    }
    
    
}

const calc = new Calculator();
let ans;